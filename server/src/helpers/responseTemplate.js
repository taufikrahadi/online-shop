const response = (status, message, data) => ({
  status,
  message,
  data
})

module.exports = response
