const Joi = require('@hapi/joi')

// Product Category validation
const productCategoriValidation = (data) => {
    const schema = Joi.object({
        name: Joi.string().min(3).required()
    });
    return schema.validate(data);
}

// Product Category validation
const userValidation = (data) => {
    const schema = Joi.object({
        firstname: Joi.string().min(3).required(),
        lastname: Joi.string().min(3).required(),
        username: Joi.string().min(3).required(),
        password: Joi.string().min(6),
        email: Joi.string().min(3).required(),
        phonenumber: Joi.string().min(3).required(),
    });
    return schema.validate(data);
}

const discountValidation = data => {
    const schema = Joi.object({
        discountName: Joi.string().min(5).required(),
        totalDiscount: Joi.number().required(),
        expiredAt: Joi.date().required()
    })
    return schema.validate(data)
}

const productValidation = data => {
    const schema = Joi.object({
        name: Joi.string().min(3).required(),
        price: Joi.number().required(),
        weight: Joi.number().required(),
        description: Joi.string().min(10).required(),
        stock: Joi.number().required(),
        categoryId: Joi.number().required(),
    })
    return schema.validate(data)
}

const addressValidation = data => {
    const schema = Joi.object({
        address: Joi.required(),
        userId: Joi.required(),
        provinceId: Joi.required(),
        cityId: Joi.required()
    })
    return schema.validate(data)
}

const orderValidation = data => {
    const schema = Joi.object({
        discountId: Joi.number(),
        orderShipmentId: Joi.number(),
        subTotal: Joi.number().required(),
        postalFee: Joi.number(),
        weightTotal: Joi.number(),
    })
    return schema.validate(data)
}

const orderDetailValidation = data => {
    const schema = Joi.object({
        productId: Joi.number().required(),
        qty: Joi.number().required(),
    })
    return schema.validate(data)
}

const variantValidation = data => {
    const schema = Joi.object({
        color: Joi.string().min(3).required(),
        size: Joi.number().required(),
        productId: Joi.required()
    })
    return schema.validate(data)
}

const adminValidation = data => {
    const schema = Joi.object({
        username: Joi.string().min(3).required(),
        password: Joi.string().min(3).required()
    })
    return schema.validate(data)
}

// module.exports.productCategoriValidation = productCategoriValidation;
// module.exports.userValidation = userValidation;
module.exports = {
    productCategoriValidation,
    userValidation,
    discountValidation,
    productValidation,
    orderDetailValidation,
    orderValidation,
    addressValidation,
    variantValidation,
    adminValidation
}
