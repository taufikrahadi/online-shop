const ejs = require("ejs")
const path = require("path");
const pdf = require("html-pdf");


module.exports = (data) => {
    ejs.renderFile(path.join(__dirname,  '../../views/', "invoice-template.ejs"), {
        data: data
    }, (err, result) => {
        if (!err) {
            const options = {
                "height": "11.25in",
                "width": "8.5in",
                "header": {
                     "height": "20mm",
                },
                "footer": {
                    "height": "20mm",
                },
            }
            pdf.create(result, options).toFile(path.join(`invoice/${data.id}.pdf`), function(err, val) {
                if (!err) {
                    console.log("File created successfully");
                } else if (err) {
                    console.log(err);
                }
            })
        } else {
            console.log(err)
        }
    })

}


