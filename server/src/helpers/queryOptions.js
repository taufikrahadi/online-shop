const { Op } = require('sequelize')

function queryOptions(where, limit, offset, include) {
  let options = {}

  if (where) options.where = where
  if (limit) options.limit = parseInt(limit)
  if (offset) options.offset = offset > 0 ? ((parseInt(offset) - 1) * parseInt(limit)) : 0
  if (include) options.include = include

  return options
}

module.exports = queryOptions
