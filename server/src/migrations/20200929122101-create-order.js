'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Orders', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userId: {
        type: Sequelize.INTEGER
      },
      total: {
        type: Sequelize.INTEGER
      },
      discountId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Discounts',
          key: 'id',
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE'
        }
      },
      orderShipmentId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'OrderShipments',
          key: 'id',
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE'
        }
      },
      subTotal: {
        type: Sequelize.INTEGER
      },
      postalFee: {
        type: Sequelize.INTEGER
      },
      weightTotal: {
        type: Sequelize.INTEGER
      },
      orderStatusId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'OrderStatuses',
          key: 'id',
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Orders');
  }
};