'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('Discounts', 'totalDiscount', {
      type: Sequelize.FLOAT,
      allowNull: false
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('Discounts', 'totalDiscount', {
      type: Sequelize.INTEGER,
      allowNull: false
    })
  }
};
