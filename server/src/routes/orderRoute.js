const router = require('express').Router()
const OrderController = require('../controllers/OrderController')

router.get('/', OrderController.fetchAll)
  .post('/', OrderController.storeData)
  .put('/:id', OrderController.updateData)
  .get('/:id', OrderController.fetchById)
  .delete('/:id', OrderController.destroyData)

module.exports = router
