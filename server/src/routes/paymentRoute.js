const PaymentController = require('../controllers/PaymentController')
const router = require('express').Router()

router.post('/', PaymentController.storeData)


module.exports = router
