const router = require('express').Router()
const DiscountController = require('../controllers/DiscountController')

router.get('/', DiscountController.fetchAll)
  .post('/', DiscountController.storeData)
  .get('/:id', DiscountController.fetchById)
  .put('/:id', DiscountController.updateData)
  .delete('/:id', DiscountController.destroyData)

module.exports = router
