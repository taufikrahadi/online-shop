const router = require('express').Router()
const AuthController = require("../controllers/AuthController")
const UserController = require('../controllers/UserController')
const AuthMiddleware = require('../middleware/AuthMiddleware')

router.post('/signup', UserController.storeData)
router.post('/login', AuthController.login)
router.get('/current-user', AuthMiddleware, AuthController.currentUser)

module.exports = router
