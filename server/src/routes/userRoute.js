const express = require('express')
const router = express.Router()

const UserController = require('../controllers/UserController')

router
  .get('/', UserController.fetchAll)
  .get('/:id', UserController.fetchById)
  .post('/', UserController.storeData)
  .put('/', UserController.updateData)
  .delete('/:id', UserController.destroyData)

module.exports = router