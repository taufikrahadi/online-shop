const router = require('express').Router()
const VariantController = require('../controllers/VariantController')

router.get('/', VariantController.fetchAll)
  .post('/', VariantController.storeData)
  .put('/:id', VariantController.updateData)
  .delete('/:id', VariantController.destroyData)

module.exports = router
