const router = require('express').Router()
const ProductController = require('../controllers/ProductController')
const upload = require('../middleware/cloudinary')
const auth = require('../middleware/AuthMiddleware')

router.get('/', ProductController.fetchAll)
  .post('/', auth, upload.single('picture'), ProductController.storeData)
  .delete('/:id', auth, ProductController.destroyData)
  .put('/:id', auth, upload.single('picture'), ProductController.updateData)
  .get('/:id', ProductController.fetchById)

module.exports = router
