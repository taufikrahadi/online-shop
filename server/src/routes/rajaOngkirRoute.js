const router = require('express').Router()
const axios = require('axios')
const https = require('https')

router.get('/provinces', async (req, res) => {
    await https.get('https://api.rajaongkir.com/starter/province', {
        headers: {
          key: '3580e5ba46d8fb1009f0afc6cd62482f'
        }
      }, resp => {
        let data = ''
    
        resp.on('data', chunk => {
          data += chunk
        })
    
        resp.on('end', () => {
          res.json(JSON.parse(data).rajaongkir)
        })
      }).on('error', () => {
        console.log('error')
      })
})


router.get('/cities/:id', async (req, res) => {
    await https.get('https://api.rajaongkir.com/starter/city?province=' + req.params.id, {
      headers: {
        key: '3580e5ba46d8fb1009f0afc6cd62482f'
      }
    }, resp => {
      let data = ''
  
      resp.on('data', chunk => {
        data += chunk
      })
  
      resp.on('end', () => {
        res.json(JSON.parse(data).rajaongkir)
      })
    }).on('error', () => {
      console.log('error')
    })
})

router.post('/cost',  (req, res) => {

    axios({
        method: 'post',
        url: 'https://api.rajaongkir.com/starter/cost',
        headers: {
            "Content-type" : "application/json",
            "Key" : "3580e5ba46d8fb1009f0afc6cd62482f"
        }, 
        data: req.body
    })    
    .then(function (response) {
        //handle success
        console.log(response.data);
        res.json(response.data)
    })
    .catch(function (response) {
        //handle error
        console.log(response);
    });
      
})


  



module.exports = router
