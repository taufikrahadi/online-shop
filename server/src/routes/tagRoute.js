const TagController = require('../controllers/TagController')
const router = require('express').Router()

router.get('/', TagController.fetchAll)
  .post('/', TagController.storeData)
  .get('/:id', TagController.fetchById)
  .put('/:id', TagController.updateData)
  .delete('/:id', TagController.destroyData)

module.exports = router
