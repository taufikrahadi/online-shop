const router = require('express').Router()
const OrderDetailController = require('../controllers/OrderDetailController')

router.get('/', OrderDetailController.fetchAll)
  .post('/', OrderDetailController.storeData)
  .put('/:id', OrderDetailController.updateData)
  .get('/:id', OrderDetailController.fetchById)
  .delete('/:id', OrderDetailController.destroyData)

module.exports = router
