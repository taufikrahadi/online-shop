const UserAddressController = require('../controllers/UserAddressController')
const router = require('express').Router()

router.get('/', UserAddressController.fetchAll)
  .post('/', UserAddressController.storeData)
  .get('/:id', UserAddressController.fetchById)
  .put('/:id', UserAddressController.updateData)
  .delete('/:id', UserAddressController.destroyData)

module.exports = router
