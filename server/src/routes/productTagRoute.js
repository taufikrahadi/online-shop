const router = require('express').Router()
const ProductTagController = require('../controllers/ProductTagController')

router.post('/', ProductTagController.storeData)
  .delete('/:id', ProductTagController.destroyData)
  .put('/:id', ProductTagController.updateData)
module.exports = router
