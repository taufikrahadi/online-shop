const router = require('express').Router()
const OrderStatusController = require('../controllers/OrderStatusController')

router.get('/', OrderStatusController.fetchAll)
  .post('/', OrderStatusController.storeData)
  .put('/:id', OrderStatusController.updateData)
  .get('/:id', OrderStatusController.fetchById)
  .delete('/:id', OrderStatusController.destroyData)

module.exports = router
