const express = require('express')
const router = express.Router()

const CategoryController = require('../controllers/CategoryController')

router
  .get('/', CategoryController.fetchAll)
  .get('/:id', CategoryController.fetchById)
  .post('/', CategoryController.storeData)
  .put('/:id', CategoryController.updateData)
  .delete('/:id', CategoryController.destroyData)

module.exports = router