'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.Variant, {
        foreignKey: 'productId',
        as: 'variants'
      }),
      this.belongsTo(models.ProductCategory, {
        foreignKey: 'categoryId',
        as: 'category'
      }),
      this.belongsTo(models.Admin, {
        foreignKey: 'adminId',
        as: 'admin'
      }),
      this.hasMany(models.OrderDetail, {
        foreignKey: 'productId',
        as: 'orders'
      }),
      this.hasMany(models.ProductTag, {
        foreignKey: 'productId',
        as: 'tags'
      })
    }
  };
  Product.init({
    code: {
      type: DataTypes.UUID,
    },
    name: {
      type: DataTypes.STRING,
      validate: {
        min: 3,
      }
    },
    price: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: true
      }
    },
    weight: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: true
      }
    },
    description: DataTypes.TEXT,
    stock: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: true
      }
    },
    adminId: DataTypes.INTEGER,
    categoryId: DataTypes.INTEGER,
    picture: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Product',
    defaultScope: {
      include: [
        'category',
        'variants',
        'tags'
      ]
    }
  });
  return Product;
};