'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.Address, {
        foreignKey: 'userId',
        as: 'address',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      }),
      this.hasMany(models.Order, {
        foreignKey: 'userId',
        as: 'orders',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    }
  };
  User.init({
    firstname: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        min: 3,
        isAlphanumeric: true
      }
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        min: 3,
        isAlphanumeric: true
      }
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        min: 3,
        isAlphanumeric: true
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
          isEmail: true
        }
    },
    phonenumber: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        min: 3,
        isAlphanumeric: true
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        min: 3
      }
    },
    salt: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};