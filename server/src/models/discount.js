'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Discount extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.Order, {
        foreignKey: 'discountId',
      })
    }
  };
  Discount.init({
    discountName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        min: 5,
        isAlphanumeric: true,
        
      }
    },
    totalDiscount: {
      type: DataTypes.FLOAT,
      allowNull: false,
      validate: {
        isFloat: true,
      }
    },
    expiredAt: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        isDate: true,
      }
    }
  }, {
    sequelize,
    modelName: 'Discount',
  });
  return Discount;
};