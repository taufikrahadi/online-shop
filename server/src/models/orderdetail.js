'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class OrderDetail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Order, {
        foreignKey: 'orderId',
        as: 'order'
      }),
      this.belongsTo(models.Product, {
        foreignKey: 'productId',
        as: 'product'
      })
    }
  };
  OrderDetail.init({
    productId: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: true
      }
    },
    qty: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: true
      }
    },
    totalPrice: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: true
      }
    },
    orderId: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: true
      }
    },
    weightTotal: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: true
      }
    }
  }, {
    sequelize,
    modelName: 'OrderDetail',
    defaultScope: {
      include: [ 'product' ]
    }
  });
  return OrderDetail;
};