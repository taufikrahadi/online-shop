'use strict';
const {
  Model
} = require('sequelize');
const { v4: uuidv4 } = require('uuid')
module.exports = (sequelize, DataTypes) => {
  class Payment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Order, {
        foreignKey: 'orderId',
        as: 'order'
      })
    }
  };
  Payment.init({
    code: {
      type: DataTypes.STRING,
      defaultValue: uuidv4()
    },
    status: DataTypes.STRING,
    gross_amount: DataTypes.INTEGER,
    transaction_token: DataTypes.UUID,
    payment_methods: DataTypes.STRING,
    orderId: DataTypes.INTEGER,
    payment_code: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Payment',
  });
  return Payment;
};