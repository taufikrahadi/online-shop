'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductTag extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Product, {
        foreignKey: 'productId',
        as: 'product'
      }),
      this.belongsTo(models.Tag, {
        foreignKey: 'tagId',
        as: 'tag'
      })
    }
  };
  ProductTag.init({
    productId: DataTypes.INTEGER,
    tagId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'ProductTag',
    defaultScope: {
      include: [
        'tag',
      ]
    }
  });
  return ProductTag;
};