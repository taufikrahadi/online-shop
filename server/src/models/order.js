'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.OrderDetail, {
        foreignKey: 'orderId',
        as: 'details'
      }),
      this.belongsTo(models.Discount, {
        foreignKey: 'discountId',
        as: 'discount'
      }),
      this.belongsTo(models.OrderShipment, {
        foreignKey: 'orderShipmentId',
        as: 'orderShipment'
      }),
      this.belongsTo(models.User, {
        foreignKey: 'userId',
        as: 'user'
      }),
      this.hasOne(models.Payment, {
        foreignKey: 'orderId',
        as: 'orderPayment'
      }),
      this.belongsTo(models.OrderStatus, {
        foreignKey: 'orderStatusId',
        as: 'orderStatus'
      })
    }
  };
  Order.init({
    userId: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: true
      }
    },
    total: {
      type: DataTypes.INTEGER,
      // validate: {
      //   isNumeric: true
      // }
    },
    discountId: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: true
      }
    },
    orderShipmentId: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: true
      }
    },
    subTotal: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: true
      }
    },
    postalFee: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: true
      }
    },
    weightTotal: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: true
      }
    },
    orderStatusId: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
      validate: {
        isNumeric: true
      }
    }
  }, {
    sequelize,
    modelName: 'Order',
  });
  return Order;
};