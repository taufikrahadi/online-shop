'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   return queryInterface.bulkInsert('ProductTags', 
   [
     {
       productId: 1,
       tagId: 1,
       createdAt: new Date(),
       updatedAt: new Date()
     },
     {
       productId: 2,
       tagId: 2,
       createdAt: new Date(),
       updatedAt: new Date()
     },
     {
      productId: 2,
      tagId: 3,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      productId: 2,
      tagId: 4,
      createdAt: new Date(),
      updatedAt: new Date()
    },
   ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
