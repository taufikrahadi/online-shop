'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   return queryInterface.bulkInsert('Payments', 
   [
     {
       code: 1,
       status: 'success',
       gross_amount: 1,
       transaction_token: 1234567,
       payment_methods: 'transfer',
       orderId: 1,
       createdAt: new Date(),
       updatedAt: new Date()
     },
     {
       code: 2,
       status: 'success',
       gross_amount: 2,
       transaction_token: 1234567,
       payment_methods: 'transfer',
       orderId: 2,
       createdAt: new Date(),
       updatedAt: new Date()
     },
   ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
