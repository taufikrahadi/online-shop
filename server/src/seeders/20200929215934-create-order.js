'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   return queryInterface.bulkInsert('Orders', 
   [
     {
       userId: 1,
       total: 10000,
       discountId: 1,
       orderShipmentId: 1,
       subTotal: 1,
       postalFee: 1,
       weightTotal: 20,
       orderStatusId: 1,
       createdAt: new Date(),
       updatedAt: new Date()
     },
     {
       userId: 2,
       total: 20000,
       discountId: 2,
       orderShipmentId: 2,
       subTotal: 2,
       postalFee: 2,
       weightTotal: 20,
       orderStatusId: 2,
       createdAt: new Date(),
       updatedAt: new Date()
     },
   ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
