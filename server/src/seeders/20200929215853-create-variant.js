'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   return queryInterface.bulkInsert('Variants', 
   [
     {
       color: 'red',
       size: "3",
       productId: 1,
       createdAt: new Date(),
       updatedAt: new Date()
     },
     {
       color: 'blue',
       size: '3',
       productId: 2,
       createdAt: new Date(),
       updatedAt: new Date()
     },
   ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
