'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   return queryInterface.bulkInsert('OrderDetails', 
   [
     {
       productId: 1,
       qty: 1,
       totalPrice: 10000,
       orderId: 1,
       weightTotal: 10,
       createdAt: new Date(),
       updatedAt: new Date()
     },
     {
       productId: 2,
       qty: 2,
       totalPrice: 30000,
       orderId: 2,
       weightTotal: 10,
       createdAt: new Date(),
       updatedAt: new Date()
     },
   ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
