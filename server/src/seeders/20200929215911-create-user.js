'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   return queryInterface.bulkInsert('Users', 
   [
     {
       firstName: 'John',
       lastName: 'Doe',
       username: 'john doe',
       email: 'example@example.com',
       phoneNumber: '087738934282',
       password: 'passjohn',
       salt:' ',
       createdAt: new Date(),
       updatedAt: new Date()
     },
     {
       firstName: 'Sam',
       lastName: 'Doe',
       username: 'sam doe',
       email: 'example@example.com',
       phoneNumber: '087738934282',
       password: 'passsam',
       salt:' ',
       createdAt: new Date(),
       updatedAt: new Date()
     },

   ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
