"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    return queryInterface.bulkInsert("Products", [
      {
        name: "product 2",
        price: 1000,
        weight: 10,
        description: "this is desceription",
        stock: 10,
        adminId: 2,
        categoryId: 2,
        picture: "sdsdsds.png",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Tasty Soft Chair",
        price: 812200,
        weight: 8,
        description:
          "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016",
        stock: 588,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/5ab5db/000&text=Tasty Soft Chair",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Practical Fresh Keyboard",
        price: 835300,
        weight: 8,
        description:
          "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles",
        stock: 800,
        adminId: 2,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/ed96a2/000&text=Practical Fresh Keyboard",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Tasty Cotton Car",
        price: 338600,
        weight: 6,
        description:
          "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit",
        stock: 158,
        adminId: 1,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/4fcecd/000&text=Tasty Cotton Car",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Ergonomic Concrete Soap",
        price: 633900,
        weight: 1,
        description:
          "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality",
        stock: 613,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/91732e/000&text=Ergonomic Concrete Soap",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Generic Metal Cheese",
        price: 58100,
        weight: 3,
        description:
          "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals",
        stock: 338,
        adminId: 1,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/889c7b/000&text=Generic Metal Cheese",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Generic Steel Chips",
        price: 670900,
        weight: 10,
        description:
          "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality",
        stock: 56,
        adminId: 1,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/a12535/000&text=Generic Steel Chips",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Ergonomic Wooden Sausages",
        price: 164600,
        weight: 2,
        description:
          "The Football Is Good For Training And Recreational Purposes",
        stock: 931,
        adminId: 1,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/9c058f/000&text=Ergonomic Wooden Sausages",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Ergonomic Soft Keyboard",
        price: 471900,
        weight: 3,
        description:
          "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive",
        stock: 632,
        adminId: 1,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/e9f305/000&text=Ergonomic Soft Keyboard",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Rustic Plastic Computer",
        price: 856100,
        weight: 4,
        description:
          "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients",
        stock: 46,
        adminId: 2,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/ebd930/000&text=Rustic Plastic Computer",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Sleek Rubber Ball",
        price: 434600,
        weight: 5,
        description:
          "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality",
        stock: 505,
        adminId: 1,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/797f3a/000&text=Sleek Rubber Ball",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Ergonomic Fresh Shirt",
        price: 276300,
        weight: 10,
        description:
          "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles",
        stock: 971,
        adminId: 1,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/9ab905/000&text=Ergonomic Fresh Shirt",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Small Rubber Table",
        price: 771500,
        weight: 2,
        description:
          "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support",
        stock: 155,
        adminId: 2,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/95ac81/000&text=Small Rubber Table",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Incredible Frozen Soap",
        price: 194600,
        weight: 1,
        description:
          "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals",
        stock: 915,
        adminId: 1,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/4af086/000&text=Incredible Frozen Soap",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Handcrafted Steel Fish",
        price: 649700,
        weight: 9,
        description:
          "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support",
        stock: 879,
        adminId: 1,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/ef633b/000&text=Handcrafted Steel Fish",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Practical Granite Car",
        price: 474400,
        weight: 4,
        description:
          "The Football Is Good For Training And Recreational Purposes",
        stock: 617,
        adminId: 1,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/b7db9e/000&text=Practical Granite Car",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Incredible Wooden Pants",
        price: 624100,
        weight: 1,
        description:
          "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive",
        stock: 624,
        adminId: 2,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/eb51c4/000&text=Incredible Wooden Pants",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Ergonomic Steel Shoes",
        price: 301000,
        weight: 7,
        description:
          "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients",
        stock: 48,
        adminId: 2,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/a425f0/000&text=Ergonomic Steel Shoes",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Sleek Plastic Computer",
        price: 279000,
        weight: 5,
        description:
          "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support",
        stock: 345,
        adminId: 2,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/e2131a/000&text=Sleek Plastic Computer",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Fantastic Rubber Ball",
        price: 775700,
        weight: 4,
        description:
          "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals",
        stock: 744,
        adminId: 1,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/2ccd4a/000&text=Fantastic Rubber Ball",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Licensed Plastic Chicken",
        price: 625800,
        weight: 1,
        description:
          "The Football Is Good For Training And Recreational Purposes",
        stock: 282,
        adminId: 1,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/2749b1/000&text=Licensed Plastic Chicken",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Rustic Metal Bacon",
        price: 664100,
        weight: 7,
        description:
          "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support",
        stock: 515,
        adminId: 2,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/4b31a3/000&text=Rustic Metal Bacon",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Refined Wooden Cheese",
        price: 904700,
        weight: 5,
        description:
          "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016",
        stock: 905,
        adminId: 1,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/b6c809/000&text=Refined Wooden Cheese",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Incredible Granite Bacon",
        price: 847500,
        weight: 10,
        description:
          "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart",
        stock: 892,
        adminId: 1,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/b01872/000&text=Incredible Granite Bacon",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Licensed Soft Salad",
        price: 234600,
        weight: 9,
        description:
          "The Football Is Good For Training And Recreational Purposes",
        stock: 143,
        adminId: 1,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/6ad874/000&text=Licensed Soft Salad",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Unbranded Fresh Shirt",
        price: 671300,
        weight: 3,
        description:
          "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality",
        stock: 366,
        adminId: 2,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/94dc85/000&text=Unbranded Fresh Shirt",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Gorgeous Rubber Pizza",
        price: 712900,
        weight: 6,
        description:
          "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality",
        stock: 875,
        adminId: 2,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/354d51/000&text=Gorgeous Rubber Pizza",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Refined Plastic Mouse",
        price: 90200,
        weight: 7,
        description:
          "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart",
        stock: 422,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/a648d4/000&text=Refined Plastic Mouse",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Sleek Granite Ball",
        price: 347900,
        weight: 5,
        description:
          "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016",
        stock: 796,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/158f18/000&text=Sleek Granite Ball",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Fantastic Fresh Tuna",
        price: 905500,
        weight: 5,
        description:
          "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles",
        stock: 934,
        adminId: 1,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/16d5f9/000&text=Fantastic Fresh Tuna",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Refined Wooden Towels",
        price: 537500,
        weight: 1,
        description:
          "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart",
        stock: 179,
        adminId: 1,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/7857e3/000&text=Refined Wooden Towels",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Ergonomic Cotton Hat",
        price: 733100,
        weight: 5,
        description:
          "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive",
        stock: 258,
        adminId: 1,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/8ab109/000&text=Ergonomic Cotton Hat",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Gorgeous Rubber Pizza",
        price: 573000,
        weight: 4,
        description:
          "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients",
        stock: 847,
        adminId: 1,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/faf7d7/000&text=Gorgeous Rubber Pizza",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Unbranded Fresh Pizza",
        price: 884600,
        weight: 5,
        description:
          "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J",
        stock: 528,
        adminId: 2,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/211201/000&text=Unbranded Fresh Pizza",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Unbranded Wooden Soap",
        price: 419400,
        weight: 8,
        description:
          "The Football Is Good For Training And Recreational Purposes",
        stock: 604,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/8d82fb/000&text=Unbranded Wooden Soap",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Tasty Frozen Shoes",
        price: 284100,
        weight: 4,
        description:
          "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive",
        stock: 742,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/9b1783/000&text=Tasty Frozen Shoes",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Tasty Wooden Chips",
        price: 42200,
        weight: 5,
        description:
          "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive",
        stock: 3,
        adminId: 2,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/c7f964/000&text=Tasty Wooden Chips",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Refined Rubber Car",
        price: 331700,
        weight: 2,
        description:
          "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart",
        stock: 775,
        adminId: 1,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/e23dc8/000&text=Refined Rubber Car",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Licensed Frozen Sausages",
        price: 512200,
        weight: 7,
        description:
          "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support",
        stock: 693,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/d5ffe4/000&text=Licensed Frozen Sausages",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Handmade Frozen Chicken",
        price: 733900,
        weight: 9,
        description:
          "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J",
        stock: 356,
        adminId: 1,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/533d40/000&text=Handmade Frozen Chicken",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Licensed Cotton Shirt",
        price: 537700,
        weight: 9,
        description:
          "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit",
        stock: 835,
        adminId: 1,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/ae83f7/000&text=Licensed Cotton Shirt",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Generic Cotton Gloves",
        price: 608600,
        weight: 4,
        description:
          "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design",
        stock: 368,
        adminId: 1,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/786c1b/000&text=Generic Cotton Gloves",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Incredible Wooden Chicken",
        price: 854400,
        weight: 6,
        description:
          "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals",
        stock: 513,
        adminId: 2,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/705920/000&text=Incredible Wooden Chicken",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Gorgeous Soft Bacon",
        price: 401700,
        weight: 10,
        description:
          "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J",
        stock: 556,
        adminId: 1,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/cea97d/000&text=Gorgeous Soft Bacon",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Ergonomic Frozen Fish",
        price: 328400,
        weight: 10,
        description:
          "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals",
        stock: 18,
        adminId: 2,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/af37f8/000&text=Ergonomic Frozen Fish",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Handcrafted Concrete Chicken",
        price: 246200,
        weight: 2,
        description:
          "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit",
        stock: 207,
        adminId: 1,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/c36e7a/000&text=Handcrafted Concrete Chicken",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Tasty Wooden Bacon",
        price: 596200,
        weight: 2,
        description:
          "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles",
        stock: 837,
        adminId: 2,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/ef81c8/000&text=Tasty Wooden Bacon",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Small Steel Bacon",
        price: 339100,
        weight: 3,
        description:
          "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016",
        stock: 369,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/6fa60e/000&text=Small Steel Bacon",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Unbranded Concrete Ball",
        price: 528200,
        weight: 10,
        description:
          "The Football Is Good For Training And Recreational Purposes",
        stock: 205,
        adminId: 2,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/674df6/000&text=Unbranded Concrete Ball",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Handcrafted Granite Towels",
        price: 842900,
        weight: 9,
        description:
          "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016",
        stock: 395,
        adminId: 2,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/6ef560/000&text=Handcrafted Granite Towels",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Licensed Fresh Cheese",
        price: 43600,
        weight: 5,
        description:
          "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016",
        stock: 63,
        adminId: 1,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/9b827c/000&text=Licensed Fresh Cheese",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Generic Concrete Cheese",
        price: 206600,
        weight: 3,
        description:
          "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016",
        stock: 915,
        adminId: 1,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/270814/000&text=Generic Concrete Cheese",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Refined Plastic Shirt",
        price: 586400,
        weight: 2,
        description:
          "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals",
        stock: 602,
        adminId: 1,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/b0560d/000&text=Refined Plastic Shirt",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Handcrafted Plastic Hat",
        price: 274500,
        weight: 4,
        description:
          "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles",
        stock: 478,
        adminId: 1,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/b25a1b/000&text=Handcrafted Plastic Hat",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Awesome Rubber Bacon",
        price: 272900,
        weight: 1,
        description:
          "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart",
        stock: 807,
        adminId: 2,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/374891/000&text=Awesome Rubber Bacon",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Handmade Rubber Soap",
        price: 920300,
        weight: 4,
        description:
          "The Football Is Good For Training And Recreational Purposes",
        stock: 680,
        adminId: 2,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/023f36/000&text=Handmade Rubber Soap",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Practical Frozen Shoes",
        price: 921100,
        weight: 2,
        description:
          "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles",
        stock: 488,
        adminId: 2,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/254412/000&text=Practical Frozen Shoes",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Handcrafted Granite Tuna",
        price: 205600,
        weight: 1,
        description:
          "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J",
        stock: 285,
        adminId: 1,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/3fd946/000&text=Handcrafted Granite Tuna",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Unbranded Concrete Tuna",
        price: 69000,
        weight: 1,
        description:
          "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J",
        stock: 622,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/a602de/000&text=Unbranded Concrete Tuna",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Handcrafted Cotton Cheese",
        price: 605500,
        weight: 7,
        description:
          "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit",
        stock: 938,
        adminId: 1,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/60fc0e/000&text=Handcrafted Cotton Cheese",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Practical Cotton Pants",
        price: 634400,
        weight: 9,
        description:
          "The Football Is Good For Training And Recreational Purposes",
        stock: 761,
        adminId: 1,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/894940/000&text=Practical Cotton Pants",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Awesome Frozen Towels",
        price: 352500,
        weight: 5,
        description:
          "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support",
        stock: 550,
        adminId: 1,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/237749/000&text=Awesome Frozen Towels",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Intelligent Fresh Mouse",
        price: 526900,
        weight: 7,
        description:
          "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality",
        stock: 542,
        adminId: 1,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/ea2af5/000&text=Intelligent Fresh Mouse",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Handmade Metal Salad",
        price: 105100,
        weight: 10,
        description:
          "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design",
        stock: 519,
        adminId: 2,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/dbd0f8/000&text=Handmade Metal Salad",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Unbranded Wooden Chicken",
        price: 68700,
        weight: 3,
        description:
          "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit",
        stock: 858,
        adminId: 1,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/09fcb2/000&text=Unbranded Wooden Chicken",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Tasty Plastic Chicken",
        price: 504800,
        weight: 8,
        description:
          "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016",
        stock: 363,
        adminId: 2,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/653a4b/000&text=Tasty Plastic Chicken",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Tasty Wooden Shirt",
        price: 33100,
        weight: 6,
        description:
          "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality",
        stock: 534,
        adminId: 1,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/0643af/000&text=Tasty Wooden Shirt",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Awesome Granite Soap",
        price: 979400,
        weight: 6,
        description:
          "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016",
        stock: 7,
        adminId: 2,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/e05fe7/000&text=Awesome Granite Soap",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Fantastic Rubber Pants",
        price: 41800,
        weight: 5,
        description:
          "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit",
        stock: 151,
        adminId: 1,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/1bcb9d/000&text=Fantastic Rubber Pants",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Sleek Rubber Hat",
        price: 12400,
        weight: 2,
        description:
          "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles",
        stock: 716,
        adminId: 1,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/19b0e2/000&text=Sleek Rubber Hat",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Tasty Concrete Fish",
        price: 256500,
        weight: 5,
        description:
          "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles",
        stock: 364,
        adminId: 2,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/e62862/000&text=Tasty Concrete Fish",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Practical Granite Fish",
        price: 281600,
        weight: 7,
        description:
          "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality",
        stock: 867,
        adminId: 1,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/088c5e/000&text=Practical Granite Fish",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Intelligent Fresh Hat",
        price: 14500,
        weight: 10,
        description:
          "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016",
        stock: 787,
        adminId: 1,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/1dfb73/000&text=Intelligent Fresh Hat",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Fantastic Wooden Tuna",
        price: 613700,
        weight: 9,
        description:
          "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit",
        stock: 437,
        adminId: 2,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/375ec2/000&text=Fantastic Wooden Tuna",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Rustic Wooden Chips",
        price: 277300,
        weight: 8,
        description:
          "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive",
        stock: 544,
        adminId: 2,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/4abe95/000&text=Rustic Wooden Chips",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Licensed Metal Ball",
        price: 498400,
        weight: 4,
        description:
          "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design",
        stock: 565,
        adminId: 2,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/9a976d/000&text=Licensed Metal Ball",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Handmade Metal Shoes",
        price: 808900,
        weight: 6,
        description:
          "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit",
        stock: 386,
        adminId: 1,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/9d2ce4/000&text=Handmade Metal Shoes",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Handmade Rubber Gloves",
        price: 912200,
        weight: 3,
        description:
          "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart",
        stock: 939,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/93294e/000&text=Handmade Rubber Gloves",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Intelligent Rubber Gloves",
        price: 325000,
        weight: 7,
        description:
          "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support",
        stock: 702,
        adminId: 2,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/77345a/000&text=Intelligent Rubber Gloves",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Tasty Cotton Shoes",
        price: 333900,
        weight: 1,
        description:
          "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit",
        stock: 251,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/e3a62d/000&text=Tasty Cotton Shoes",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Rustic Soft Salad",
        price: 64100,
        weight: 8,
        description:
          "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart",
        stock: 371,
        adminId: 1,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/f79b7c/000&text=Rustic Soft Salad",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Intelligent Cotton Bacon",
        price: 87100,
        weight: 4,
        description:
          "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J",
        stock: 105,
        adminId: 2,
        categoryId: 4,
        picture:
          "https://dummyimage.com/300.png/7f8cbd/000&text=Intelligent Cotton Bacon",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Generic Frozen Car",
        price: 889500,
        weight: 6,
        description:
          "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design",
        stock: 943,
        adminId: 2,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/5a651f/000&text=Generic Frozen Car",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Sleek Steel Cheese",
        price: 527700,
        weight: 10,
        description:
          "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit",
        stock: 921,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/3a2142/000&text=Sleek Steel Cheese",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Rustic Soft Table",
        price: 18800,
        weight: 1,
        description:
          "The Football Is Good For Training And Recreational Purposes",
        stock: 116,
        adminId: 2,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/d4c515/000&text=Rustic Soft Table",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Refined Wooden Fish",
        price: 743200,
        weight: 4,
        description:
          "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J",
        stock: 722,
        adminId: 2,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/95dd2f/000&text=Refined Wooden Fish",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Rustic Steel Salad",
        price: 811200,
        weight: 10,
        description:
          "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support",
        stock: 940,
        adminId: 1,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/80b353/000&text=Rustic Steel Salad",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Unbranded Fresh Bike",
        price: 44800,
        weight: 9,
        description:
          "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit",
        stock: 486,
        adminId: 1,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/df3ed9/000&text=Unbranded Fresh Bike",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Sleek Wooden Shoes",
        price: 137400,
        weight: 7,
        description:
          "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles",
        stock: 900,
        adminId: 1,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/d3cac7/000&text=Sleek Wooden Shoes",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Unbranded Metal Towels",
        price: 443400,
        weight: 5,
        description:
          "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support",
        stock: 729,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/582108/000&text=Unbranded Metal Towels",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Generic Frozen Chips",
        price: 299400,
        weight: 10,
        description:
          "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart",
        stock: 159,
        adminId: 1,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/612a70/000&text=Generic Frozen Chips",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Ergonomic Cotton Salad",
        price: 514700,
        weight: 3,
        description:
          "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality",
        stock: 390,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/127a41/000&text=Ergonomic Cotton Salad",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Handcrafted Granite Bike",
        price: 951200,
        weight: 2,
        description:
          "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals",
        stock: 899,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/da0845/000&text=Handcrafted Granite Bike",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Intelligent Concrete Hat",
        price: 672800,
        weight: 2,
        description:
          "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals",
        stock: 981,
        adminId: 1,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/441197/000&text=Intelligent Concrete Hat",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Incredible Granite Soap",
        price: 461600,
        weight: 4,
        description:
          "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive",
        stock: 411,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/b477db/000&text=Incredible Granite Soap",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Handmade Soft Pizza",
        price: 293100,
        weight: 5,
        description:
          "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit",
        stock: 282,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/166dfb/000&text=Handmade Soft Pizza",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Ergonomic Cotton Shirt",
        price: 523200,
        weight: 6,
        description:
          "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality",
        stock: 184,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/35c752/000&text=Ergonomic Cotton Shirt",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Awesome Concrete Fish",
        price: 97100,
        weight: 2,
        description:
          "The Football Is Good For Training And Recreational Purposes",
        stock: 849,
        adminId: 2,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/1efe83/000&text=Awesome Concrete Fish",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Small Cotton Fish",
        price: 146700,
        weight: 10,
        description:
          "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals",
        stock: 970,
        adminId: 2,
        categoryId: 1,
        picture:
          "https://dummyimage.com/300.png/77ae73/000&text=Small Cotton Fish",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Incredible Fresh Shoes",
        price: 718700,
        weight: 8,
        description:
          "The Football Is Good For Training And Recreational Purposes",
        stock: 943,
        adminId: 2,
        categoryId: 2,
        picture:
          "https://dummyimage.com/300.png/a8b0bd/000&text=Incredible Fresh Shoes",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Rustic Metal Shoes",
        price: 276100,
        weight: 7,
        description:
          "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit",
        stock: 72,
        adminId: 2,
        categoryId: 3,
        picture:
          "https://dummyimage.com/300.png/e84926/000&text=Rustic Metal Shoes",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Products", null, {});
  },
};
