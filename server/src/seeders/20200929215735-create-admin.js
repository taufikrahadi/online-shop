'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   return queryInterface.bulkInsert('Admins', 
   [
     {
       username: 'john doe',
       password: 'passjohn',
       createdAt: new Date(),
       updatedAt: new Date()
     },
     {
       username: 'sam doe',
       password: 'passsam',
       createdAt: new Date(),
       updatedAt: new Date()
     },

   ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
