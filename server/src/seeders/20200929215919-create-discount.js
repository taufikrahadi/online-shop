'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   return queryInterface.bulkInsert('Discounts', 
   [
     {
       discountName: 'Discounts 1',
       totalDiscount: 20,
       expiredAt: new Date(),
       createdAt: new Date(),
       updatedAt: new Date()
     },
     {
       discountName: 'Discounts 2',
       totalDiscount: 30,
       expiredAt: new Date(),
       createdAt: new Date(),
       updatedAt: new Date()
     },
   ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
