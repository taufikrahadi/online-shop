'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   return queryInterface.bulkInsert('OrderShipments', 
   [
     {
       receiptNumber: 'receipt 1',
       weightTotal: 10,
       provinceId: 1,
       cityId: 1,
       createdAt: new Date(),
       updatedAt: new Date()
     },
     {
       receiptNumber: 'receipt 2',
       weightTotal: 20,
       provinceId: 2,
       cityId: 2,
       createdAt: new Date(),
       updatedAt: new Date()
     },
   ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
