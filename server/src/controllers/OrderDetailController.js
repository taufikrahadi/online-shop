const models = require("../models")
const response = require("../helpers/responseTemplate")
const queryOptions = require("../helpers/queryOptions")
const { orderDetailValidation } = require("../helpers/validation");
const createPdf = require('../helpers/createPdf')
const mustache = require("mustache")
const ejs = require("ejs")
const path = require("path");
const transporter = require("../../config/mailConfig")

const { Op } = require('sequelize')


class OrderDetailController {

    static async fetchAll(req, res) {
        
      const { search, limit, page} = req.query

      const options = queryOptions(
        search ? { // ini ternary yg nge cek apakah value search itu != undefined
          [Op.or]: [
            {
              'productId': { [Op.like]: `%${search}%` } // filter search nya dari kolom name
            }, // kalo mau nambah kolom lagi tinggal tambahin selanjutnya
            {
              'qty': { [Op.like]: `%${search}%` }    
            },
            {
              'totalPrice': { [Op.like]: `%${search}%` }    
            },
            {
              'orderId': { [Op.like]: `%${search}%` }    
            },
            {
               'weightTotal': { [Op.like]: `%${search}%` }    
            }
          ]
        } : undefined,
        limit,
        page,
            [
                {
                    model: models.Order, as: "order",
                    include: [
                      {model: models.User, as: "user"}
                    ]
                },
                {
                    model: models.Product, as: "product"
                },
            ]
      )

      try {
        const OrderDetails = await models.OrderDetail.findAndCountAll(options) // pake findAndCountAll supaya sekalian nge return total item
        const totalPage = page ? OrderDetails.count / parseInt(limit) : 1
        const total = totalPage % 1 ? parseInt(totalPage) + 1 : totalPage
        res.status(200)
          .json(
            response(
              'success',
              'data order details ',
              {
                data: OrderDetails.rows, // data nya
                totalItems: OrderDetails.count, // total data nya
                totalPages: total, // total data dibagi value query limit
                currentPage: page ? parseInt(page) : 1 // current page
              }
            )
          )
      } catch ({ message }) {
        res.json(message).status(500)      
      }
    }

    static async fetchById(req, res) {
      const { id } = req.params
      console.log(id)
      try {
        const OrderDetails = await models.OrderDetail.findOne({
          where: {
            id: id
          }
        })
  
        if (!OrderDetails) return res.status(404).json(response('failed', 'data not found'))
        res.status(200).json(response('success', 'get data by id', OrderDetails))
      } catch ({ message }) {
        return res.status(500).json(response('failed', message))
      }
    }

    static async storeData(req, res) {
      try {
        // Validasi Category
        const {
          error
          } = orderDetailValidation(req.body.data)
          if (error) return res.status(400).json(response("Failed!", error.details[0].message, ""))

        const OrderDetails = await models.OrderDetail.create({ ...req.body.data })
        if(OrderDetails) {

          const { orderId } = req.body.data

              const id = `invoice-${new Date().toISOString().split(":").join("-")}`; // nama file berdasar tangga
              const { count, rows } = await models.OrderDetail.findAndCountAll({
                where: {
                  orderId : orderId
                },
                include:          [
                  {
                      model: models.Order, as: "order",
                      include: [
                        {model: models.User, as: "user"}
                      ]
                  },
                  {
                      model: models.Product, as: "product"
                  },
              ]
              })
              createPdf({count, rows, id: id})
              const template = await ejs.renderFile(path.join(__dirname, "../../views/email-template.ejs"))
              const mailOptions = {
                from: process.env.EMAIL,
                to: 'lombokvisit98@gmail.com',
                subject: 'INVOICE ORDER',
                template: 'main',
                html: mustache.render(template),
                attachments: [{
                  filename: `${id}.pdf`, 
                  path: path.join(__dirname, '../../invoice', `${id}.pdf`),
                  contentType: 'application/pdf'
                }]

              }
              transporter.sendMail(mailOptions)
        }
        res.status(200).json(response('success', 'success create data', OrderDetails))
      } catch ({ message }) {
        res.status(500).json(response('failed', message))
      }
    }

    static async updateData(req, res) {
      const { id } = req.params
      const { 
          productId,
          qty,
          totalPrice,
          orderId,
          weightTotal 
        } = req.body.data

      try {
        const OrderDetails = await models.OrderDetail.findByPk(id)
        console.log(id)
        if(!OrderDetails) return res.json(response('failed', 'data not found')).status(404)

        // Validasi Category
        const {
          error
          } = orderDetailValidation(req.body.data)
          if (error) return res.status(400).json(response("Failed!", error.details[0].message, ""))
  
        OrderDetails.productId = productId
        OrderDetails.qty = qty
        OrderDetails.totaPrice = totalPrice
        OrderDetails.orderId = orderId
        OrderDetails.weightTotal = weightTotal
        OrderDetails.save()
        res.json(response('success', 'success update data', OrderDetails)).status(200)
      } catch ({ message }) {
        res.status(500).json(response('failed', message))
      }
    }

    static async destroyData(req, res) {
      const { id } = req.params
      try {
        const OrderDetails = await models.OrderDetail.findByPk(id)
        
        if (!OrderDetails) return res.status(404).json(response('failed', 'data not found'))

        await OrderDetails.destroy({
          where: {
            id: id
          }
        })

        res.status(200).json(response('success', 'data deleted'))
      } catch ({ message }) {
        res.status(500).json(response('failed', message))
      }
    }

}

module.exports = OrderDetailController;