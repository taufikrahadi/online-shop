const models = require('../models')
const { Op } = require('sequelize')
const queryOptions = require('../helpers/queryOptions')
const response = require('../helpers/responseTemplate')

module.exports = class OrderStatusController {
  static async fetchAll(req, res) {
    // tangkep query dari client
    const { search, limit, page, include } = req.query

    // pakai method queryOptions isi argument pertama itu statement searching nya
    // argument ke dua limit
    // argument ke tiga page / offset,
    // argument ke empat include
    /*
    TODO: map include and return object of array
    */
    const includeProps = include ? include.split(',').map(i => {
      const model = models[i]
      return { model: model }
    }) : include
    const options = queryOptions(
      search ? { // ini ternary yg nge cek apakah value search itu != undefined
        [Op.or]: [
          {
            'name': { [Op.like]: `%${search}%` } // filter search nya dari kolom name
          } // kalo mau nambah kolom lagi tinggal tambahin selanjutnya
        ]
      } : undefined,
      limit,
      page,
      includeProps
    )

    try {
      const orderStatuses = await models.OrderStatus.findAndCountAll(options) // pake findAndCountAll supaya sekalian nge return total item
      const totalPage = page ? orderStatuses.count / parseInt(limit) : 1
      const total = totalPage % 1 ? parseInt(totalPage) + 1 : totalPage
      res.status(200)
        .json(
          response(
            'success',
            'data order status',
            {
              data: orderStatuses.rows, // data nya
              totalItems: orderStatuses.count, // total data nya
              totalPages: total, // total data dibagi value query limit
              currentPage: page ? parseInt(page) : 1 // current page
            }
          )
        )
    } catch ({ message }) {
      res.json(response(message)).status(500)      
    }
  }

  static async fetchById(req, res) {
    const { id } = req.params
    console.log(id)
    try {
      const orderStatus = await models.OrderStatus.findOne({
        where: {
          id: id
        }
      })

      if (!orderStatus) return res.status(404).json(response('failed', 'data not found'))
      res.status(200).json(response('success', 'get data by id', orderStatus))
    } catch ({ message }) {
      return res.status(500).json(response('failed', message))
    }
  }

  static async storeData(req, res) {
    const { name } = req.body.data

    if (name.length < 4) return res.json({ error: 'name must be at least 4 characters' })

    try {
      const orderStatus = await models.OrderStatus.create({ ...req.body.data })
      res.status(201).json(response('success', 'success create data', orderStatus))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }

  static async updateData(req, res) {
    const { id } = req.params
    const { name } = req.body.data
    
    if (name.length < 4) return res.json({ error: 'name must be at least 4 characters' })

    try {
      const orderStatus = await models.OrderStatus.findByPk(id)
      console.log(id)
      if(!orderStatus) return res.json(response('failed', 'data not found')).status(404)

      orderStatus.name = name
      orderStatus.save()
      res.json(response('success', 'success update data', orderStatus)).status(200)
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }

  static async destroyData(req, res) {
    const { id } = req.params

    try {
      const orderStatus = await models.OrderStatus.findByPk(id)
      
      if (!orderStatus) return res.status(404).json(response('failed', 'data not found'))

      await models.OrderStatus.destroy({
        where: {
          id: id
        }
      })

      res.status(200).json(response('success', 'data deleted'))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }
}
