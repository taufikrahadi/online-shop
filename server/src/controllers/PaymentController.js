require('dotenv').config()
const midtransClient = require('midtrans-client')
// const response = require('../helpers/responseTemplate')
// const { v4: uuidv4 } = require('uuid')

class PaymentController {
  static async storeData(order, data) {
    let core = new midtransClient.CoreApi({
      isProduction : false,
      serverKey : process.env.MIDTRANS_SERVER_KEY,
      clientKey : process.env.MIDTRANS_CLIENT_KEY,
    });

    console.log(`- Received charge request:`,order, data);
    try {
      const apiResponse = await core.charge({
        "payment_type": 'cstore',
        "transaction_details": {
          "gross_amount": order.total,
          "order_id": order.id,
        },
        "cstore": {
          "store": data.store,
          "message": "Message to display"
        }
      })
      return apiResponse
    } catch (error) {
      throw new Error(error)
    }
  }

}

module.exports = PaymentController
