const models = require('../models')
const queryOptions = require('../helpers/queryOptions')
const response = require('../helpers/responseTemplate')
const { Op } = require('sequelize')

class TagController {
  static async fetchAll(req, res) {
    const { search, limit, page } = req.query

    const options = queryOptions(
      search ? {
        [Op.or]: [
          { 'name': { [Op.like]: `%${search}%` } }
        ]
      } : undefined,
      limit,
      page,
      [
        {
          model: models.ProductTag,
          include: [
            'product'
          ]
        }
      ]
    )

    try {
      const tags = await models.Tag.findAndCountAll(options)
      const totalPage = page ? tags.count / parseInt(limit) : 1
      const total = totalPage % 1 ? parseInt(totalPage) + 1 : totalPage
      res.status(200)
        .json(response(
          'success',
          'data tags',
          {
            data: tags.rows,
            totalItems: tags.count,
            totalPages: total,
            currentPage: page ? parseInt(page) : 1
          }
        )
      )
    } catch ({ message }) {
      res.json(response('failed', message))
    }
  }

  static async fetchById(req, res) {
    const { id } = req.params

    try {
      const tag = await models.Tag.findOne({
        where: {
          id: id
        },
        include: [
          {
            model: models.ProductTag,
            include: [
              { model: models.Product }
            ]
          }
        ]
      })
      res.status(200).json(response('success', 'get data by id', tag))
    } catch ({ message }) {
      res.json(response('failed', message))
    }
  }

  static async storeData(req, res) {
    if (req.body.data.name.length < 4) return res.status(422).json(response({ error: 'tag name must be at least 4 characters' })) 

    try {
      const tag = await models.Tag.create({ ...req.body.data })
      res.status(201).json(response('success', 'tag created', tag))
    } catch ({ message }) {
      res.json(response('failed', message))
    }
  }

  static async updateData(req, res) {
    if (req.body.data.name.length < 4) return res.status(422).json(response({ error: 'tag name must be at least 4 characters' })) 

    const { id } = req.params
    try {
      const tag = await models.Tag.findByPk(id)
      if (!tag) return res.status(404).json(response('failed', 'data not found'))

      tag.name = req.body.data.name
      await tag.save()
      res.status(200).json(response('success', 'data updated', tag))
    } catch ({ message }) {
      res.json(response('failed', message))
    }
  }

  static async destroyData(req, res) {
    const { id } = req.params

    try {
      const tag = await models.Tag.findByPk(id)
      if (!tag) return res.status(404).json(response('failed', 'data not found'))

      await models.Tag.destroy({
        where: {
          id: id
        }
      })

      res.status(200).json(response('success', 'data deleted'))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }
}

module.exports = TagController
