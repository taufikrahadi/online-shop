const models = require("../models")
const response = require("../helpers/responseTemplate")
const queryOptions = require("../helpers/queryOptions")
const { addressValidation } = require("../helpers/validation");
const { Op } = require('sequelize')


class UserAddressController {

    static async fetchAll(req, res) {
        
      const { search, limit, page} = req.query

      const options = queryOptions(
        search ? { // ini ternary yg nge cek apakah value search itu != undefined
          [Op.or]: [
            {
              'userId': { [Op.like]: `%${search}%` } // filter search nya dari kolom name
            }, // kalo mau nambah kolom lagi tinggal tambahin selanjutnya
            {
              'address': { [Op.like]: `%${search}%` }    
            },
            {
              'provinceId': { [Op.like]: `%${search}%` }    
            },
            {
              'cityId': { [Op.like]: `%${search}%` }    
            }
          ]
        } : undefined,
        limit,
        page,
            {
                model: models.User, as: "user",
            }
      )

      try {
        const UserAddresses = await models.Address.findAndCountAll(options) // pake findAndCountAll supaya sekalian nge return total item
        const totalPage = page ? UserAddresses.count / parseInt(limit) : 1
        const total = totalPage % 1 ? parseInt(totalPage) + 1 : totalPage
        res.status(200)
          .json(
            response(
              'success',
              'data address ',
              {
                data: UserAddresses.rows, // data nya
                totalItems: UserAddresses.count, // total data nya
                totalPages: total, // total data dibagi value query limit
                currentPage: page ? parseInt(page) : 1 // current page
              }
            )
          )
      } catch ({ message }) {
        res.json(message).status(500)      
      }
    }

    static async fetchById(req, res) {
      const { id } = req.params
      console.log(id)
      try {
        const UserAddresses = await models.Address.findOne({
          where: {
            id: id
          },
          include : {
            model: models.User, as: "user",
        }
        })
  
        if (!UserAddresses) return res.status(404).json(response('failed', 'data not found'))
        res.status(200).json(response('success', 'get data by id', UserAddresses))
      } catch ({ message }) {
        return res.status(500).json(response('failed', message))
      }
    }

    static async storeData(req, res) {
      try {
        // Validasi Category
        const {
          error
          } = addressValidation(req.body.data)
          if (error) return res.status(400).json(response("Failed!", error.details[0].message, ""))

        const UserAddresses = await models.Address.create({ ...req.body.data })
        res.status(200).json(response('success', 'success create data', UserAddresses))
      } catch ({ message }) {
        res.status(500).json(response('failed', message))
      }
    }

    static async updateData(req, res) {
      const { id } = req.params
      const { 
          userId,
          address,
          provinceId,
          cityId 
        } = req.body.data

      try {
        const UserAddresses = await models.Address.findByPk(id)
        console.log(id)
        if(!UserAddresses) return res.json(response('failed', 'data not found')).status(404)

        // Validasi Category
        const {
          error
          } = addressValidation(req.body.data)
          if (error) return res.status(400).json(response("Failed!", error.details[0].message, ""))
  
        UserAddresses.userid = userId
        UserAddresses.address = address
        UserAddresses.provinceId = provinceId
        UserAddresses.cityId = cityId
        UserAddresses.save()
        res.json(response('success', 'success update data', UserAddresses)).status(200)
      } catch ({ message }) {
        res.status(500).json(response('failed', message))
      }
    }

    static async destroyData(req, res) {
      const { id } = req.params
      try {
        const UserAddresses = await models.Address.findByPk(id)
        
        if (!UserAddresses) return res.status(404).json(response('failed', 'data not found'))

        await UserAddresses.destroy({
          where: {
            id: id
          }
        })

        res.status(200).json(response('success', 'data deleted'))
      } catch ({ message }) {
        res.status(500).json(response('failed', message))
      }
    }

}

module.exports = UserAddressController;