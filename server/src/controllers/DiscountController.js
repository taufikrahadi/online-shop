const models = require('../models')
const queryOptions = require('../helpers/queryOptions')
const response = require('../helpers/responseTemplate')
const { discountValidation } = require('../helpers/validation')
const { Op } = require('sequelize')

class DiscountController {
  static async fetchAll(req, res) {
    const { search, limit, page, include } = req.query

    const includeProps = include ? include
                                    .split(',')
                                    .map(i => {
                                      return { model: models[i] }
                                    }) : include

    const options = queryOptions(
      search ? {
        [Op.or]: [
          {
            'discountName': { [Op.like]: `%${search}%` }
          },
          {
            'expiredAt': { [Op.like]: `%${search}%` }
          }
        ]
      } : search,
      limit,
      page,
      includeProps
    )

    try {
      const discounts = await models.Discount.findAndCountAll(options)
      const totalPage = page ? discounts.count / parseInt(limit) : 1
      const total = totalPage % 1 ? parseInt(totalPage) + 1 : totalPage
      res.status(200).json(
        response(
          'success',
          'discounts data',
          {
            data: discounts.rows,
            totalItem: discounts.count,
            totalPages: total,
            currentPage: page ? parseInt(page) : 1
          }
        )
      )
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }

  static async fetchById(req, res) {
    const { id } = req.params

    try {
      const discount = await models.Discount.findOne({
        where: {
          id: id
        },
        include: [
          {
            model: models.Order
          }
        ]
      })

      res.status(200).json(response('success', 'get data by id', discount))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }

  static async storeData(req, res) {
    const { error } = discountValidation(req.body.data)
    if (error) return res.status(422).json(response('failed', 'error', error.details[0].message))

    req.body.data.totalDiscount = req.body.data.totalDiscount / 100
    req.body.data.discountName = req.body.data.discountName.toUpperCase()

    try {
      const discount = await models.Discount.create({ ...req.body.data })
      res.status(201).json(response('success', 'success created data', discount))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }

  static async updateData(req, res) {
    const { error } = discountValidation(req.body.data)
    if (error) return res.status(422).json(response('failed', 'error', error.details[0].message))

    req.body.data.totalDiscount = req.body.data.totalDiscount / 100
    req.body.data.discountName = req.body.data.discountName.toUpperCase()
    
    const { discountName, totalDiscount, expiredAt } = req.body.data
    const { id } = req.params

    try {
      const discount = await models.Discount.findByPk(id)
      if (!discount) return res.status(404).json(response('failed', 'data not found'))
      
      discount.discountName = discountName
      discount.totalDiscount = totalDiscount
      discount.expiredAt = expiredAt
      await discount.save()

      res.status(200).json(response('success', 'data updated', discount))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }

  static async destroyData(req, res) {
    const { id } = req.params

    try {
      const discount = await models.Discount.findByPk(id)
      if (!discount) return res.status(404).json(response('failed', 'data not found'))

      await models.Discount.destroy({
        where: {
          id: id
        }
      })

      res.status(200).json(response('success', 'data deleted'))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }
}

module.exports = DiscountController
