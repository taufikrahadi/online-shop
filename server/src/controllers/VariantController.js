const models = require('../models')
const queryOptions = require('../helpers/queryOptions')
const response = require('../helpers/responseTemplate')
const { Op } = require('sequelize')
const { variantValidation } = require('../helpers/validation')

class VariantController {
  static async fetchAll(req, res) {
    try {
      const variants = await models.Variant.findAll()
      res.status(200).json(response('success', 'variants product data', variants))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }

  static async storeData(req, res) {
    const { error } = variantValidation(req.body.data)
    if (error) return res.status(422).json(response('failed', error.details[0].message))
    try {
      const variant = await models.Variant.create({ ...req.body.data })
      res.status(201).json(response('success', 'success created data', variant))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }

  static async updateData(req, res) {
    const { id } = req.params
    const { error } = variantValidation(req.body.data)
    if (error) return res.status(422).json(response('failed', error.details[0].message))
    
    const { color, size, productId } = req.body.data
    try {
      const variant = await models.Variant.findByPk(id)
      if (!variant) return res.status(404).json(response('failed', 'data not found'))

      variant.color = color
      variant.size = size
      variant.productId = productId
      await variant.save()

      res.status(200).json(response('success', 'data updated', variant))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }

  static async destroyData(req, res) {
    const { id } = req.params
    try {
      const variant = await models.Variant.findByPk(id)
      if (!variant) return res.status(404).json(response('failed', 'datanot found'))

      await models.Variant.destroy({
        where: {
          id: id
        }
      })

      res.status(200).json(response('success', 'data deleted'))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }
}

module.exports = VariantController
