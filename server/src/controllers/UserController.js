const models = require("../models")
const response = require("../helpers/responseTemplate")
const queryOptions = require("../helpers/queryOptions")
const bcrypt = require("bcrypt");
const { userValidation } = require("../helpers/validation");
const { Op } = require('sequelize')

class CategoryController {

    static async fetchAll(req, res) {
      const { search, limit, page, include } = req.query
      const includeProps = include ? include.split(',') : include

      const options = queryOptions(
        search ? { // ini ternary yg nge cek apakah value search itu != undefined
          [Op.or]: [
            {
              'firstname': { [Op.like]: `%${search}%` } // filter search nya dari kolom name
            }, // kalo mau nambah kolom lagi tinggal tambahin selanjutnya
            {
              'lastname': { [Op.like]: `%${search}%` }
            },
            {
              'username': { [Op.like]: `%${search}%` }
            },
            {
              'email': { [Op.like]: `%${search}%` }
            }
          ]
        } : undefined,
        limit,
        page,
        includeProps
      )

      try {
        const Users = await models.User.findAndCountAll({ ...options, attributes: [
          'id', 'firstname', 'lastname', 'email', 'username', 'phonenumber'
        ] }) // pake findAndCountAll supaya sekalian nge return total item
        const totalPage = page ? Users.count / parseInt(limit) : 1
        const total = totalPage % 1 ? parseInt(totalPage) + 1 : totalPage
        res.status(200)
          .json(
            response(
              'success',
              'data order status',
              {
                data: Users.rows, // data nya
                totalItems: Users.count, // total data nya
                totalPages: total, // total data dibagi value query limit
                currentPage: page ? parseInt(page) : 1 // current page
              }
            )
          )
      } catch ({ message }) {
        res.json(message).status(500)      
      }
    }

    static async fetchById(req, res) {
      const { id } = req.params
      console.log(id)
      try {
        const user = await models.User.findOne({
          where: {
            id: id
          }
        })
  
        if (!user) return res.status(404).json(response('failed', 'data not found'))
        res.status(200).json(response('success', 'get data by id', user))
      } catch ({ message }) {
        return res.status(500).json(response('failed', message))
      }
    }

    static async storeData(req, res) {

      const {
        firstname,
        lastname,
        username,
        email,
        phonenumber,
        password
      } = req.body.data

          // Hash password
      const salt = bcrypt.genSaltSync(10);
      const hashedPassword = bcrypt.hashSync(password, salt)


      try {
        // Validasi User
        const {
          error
          } = userValidation(req.body.data)
          if (error) return res.status(400).json(response("Failed!", error.details[0].message, ""))
          
        const user = await models.User.create({
          firstname,
          lastname,
          username,
          email,
          phonenumber,
          password: hashedPassword,
          salt
        })

        res.status(200).json(response('success', 'success create data', user))
      } catch ({ message }) {
        res.status(500).json(response('failed', message))
      }
    }

    static async updateData(req, res) {
      const id = req.user_id;
  
      const { 
          firstname,
          lastname,
          username,
          email,
          phonenumber,
          password,
          salt
      } = req.body.data


      try {
        const user = await models.User.findByPk(id)
        if(!user) return res.json(response('failed', 'data not found')).status(404)

          // Validasi User
          const {
            error
            } = userValidation(req.body.data)
            if (error) return res.status(400).json(response("Failed!", error.details[0].message))

            user.firstname = firstname,
            user.lastname = lastname,
            user.username = username,
            user.email = email,
            user.phonenumber = phonenumber,
            user.password = password,
            user.salt = salt
            user.save()

            res.json(response('success', 'success update data', user)).status(200)
          } catch ({ message }) {
            res.status(500).json(response('failed', message))
        }
    }

    static async destroyData(req, res) {
      const { id } = req.params

      try {
        const user = await models.User.findByPk(id)
        
        if (!user) return res.status(404).json(response('failed', 'data not found'))
  
        await models.User.destroy({
          where: {
            id: id
          }
        })
  
        res.status(200).json(response('success', 'data deleted'))
      } catch ({ message }) {
        res.status(500).json(response('failed', message))
      }
    }

}

module.exports = CategoryController;