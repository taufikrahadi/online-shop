const { ProductCategory } = require("../models")
const response = require("../helpers/responseTemplate")
const queryOptions = require("../helpers/queryOptions")
const { productCategoriValidation } = require("../helpers/validation");
const { Op } = require('sequelize')


class CategoryController {

    static async fetchAll(req, res) {
      const { search, limit, page, include } = req.query
      const options = queryOptions(
        search ? { // ini ternary yg nge cek apakah value search itu != undefined
          [Op.or]: [
            {
              'name': { [Op.like]: `%${search}%` } // filter search nya dari kolom name
            } // kalo mau nambah kolom lagi tinggal tambahin selanjutnya
          ]
        } : undefined,
        limit,
        page,
        include
      )

      try {
        const ProductCategories = await ProductCategory.findAndCountAll(options) // pake findAndCountAll supaya sekalian nge return total item
        const totalPage = page ? ProductCategories.count / parseInt(limit) : 1
        const total = totalPage % 1 ? parseInt(totalPage) + 1 : totalPage
        res.status(200)
          .json(
            response(
              'success',
              'data order status',
              {
                data: ProductCategories.rows, // data nya
                totalItems: ProductCategories.count, // total data nya
                totalPages: total, // total data dibagi value query limit
                currentPage: page ? parseInt(page) : 1 // current page
              }
            )
          )
      } catch ({ message }) {
        res.json(message).status(500)      
      }
    }

    static async fetchById(req, res) {
      const { id } = req.params
      console.log(id)
      try {
        const ProductCategories = await ProductCategory.findOne({
          where: {
            id: id
          }
        })
  
        if (!ProductCategories) return res.status(404).json(response('failed', 'data not found'))
        res.status(200).json(response('success', 'get data by id', ProductCategories))
      } catch ({ message }) {
        return res.status(500).json(response('failed', message))
      }
    }

    static async storeData(req, res) {
      try {
        // Validasi Category
        const {
          error
          } = productCategoriValidation(req.body.data)
          if (error) return res.status(400).json(response("Failed!", error.details[0].message, ""))

        const ProductCategories = await ProductCategory.create({ ...req.body.data })
        res.status(200).json(response('success', 'success create data', ProductCategories))
      } catch ({ message }) {
        res.status(500).json(response('failed', message))
      }
    }

    static async updateData(req, res) {
      const { id } = req.params
      const { name } = req.body.data

      try {
        const ProductCategories = await ProductCategory.findByPk(id)
        console.log(id)
        if(!ProductCategories) return res.json(response('failed', 'data not found')).status(404)

        // Validasi Category
        const {
          error
          } = productCategoriValidation(req.body.data)
          if (error) return res.status(400).json(response("Failed!", error.details[0].message, ""))
  
        ProductCategories.name = name
        ProductCategories.save()
        res.json(response('success', 'success update data', ProductCategories)).status(200)
      } catch ({ message }) {
        res.status(500).json(response('failed', message))
      }
    }

    static async destroyData(req, res) {
      const { id } = req.params
      try {
        const ProductCategories = await ProductCategory.findByPk(id)
        
        if (!ProductCategories) return res.status(404).json(response('failed', 'data not found'))

        await ProductCategories.destroy({
          where: {
            id: id
          }
        })

        res.status(200).json(response('success', 'data deleted'))
      } catch ({ message }) {
        res.status(500).json(response('failed', message))
      }
    }

}

module.exports = CategoryController;