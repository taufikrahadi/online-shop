const models = require('../models')
const response = require('../helpers/responseTemplate')

class ProductTagController {
  static async storeData(req, res) {
    const { tagId, productId } = req.body.data

    try {
      const productTag = await models.ProductTag.create({
        productId: productId,
        tagId: tagId
      })

      res.status(201).json(response('success', 'product tag created', productTag))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }

  static async updateData(req, res) {
    const { tagId, productId } = req.body.data
    const { id } = req.params
    try {
      const productTag = await models.ProductTag.update({
        tagId, productId
      },{
        where: {
          id: id
        }
      })
      res.status(200).json(response('success', 'data update', productTag))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }

  static async destroyData(req, res) {
    const { id } = req.params

    try {
      const productTag = await models.ProductTag.findByPk(id)
      if (!productTag) return res.status(404).json(response('failed', 'data not found'))

      await models.ProductTag.destroy({
        where: {
          id: id
        }
      })

      res.status(200).json(response('success', 'data deleted'))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }
}

module.exports = ProductTagController
