require('dotenv').config()
const models = require("../models")
const response = require('../helpers/responseTemplate')
const jwt = require('jsonwebtoken')
const bcrypt = require("bcrypt")
const { adminValidation } = require('../helpers/validation')
const jwtSecret = process.env.SECRET

class Controller {

  static async login(req, res) {
    const { data } = req.body;
    console.log({ data });
    try {
      const admin = await models.Admin.findOne({
        where: {
          username: req.body.data.username,
        },
        raw: true,
      });

      console.log(admin)

      if (!admin) {
        return res
          .status(422)
          .json(response("Fail", "Username or password not found"));
      }

      if (!bcrypt.compareSync(req.body.data.password, admin.password)) {
        return res
          .status(422)
          .json(response("Fail", "Username or password not found"));
      }
      const token = jwt.sign(admin.id, jwtSecret);

      delete admin.password;
      delete admin.createdAt;
      delete admin.updatedAt;
      res
        .status(200)
        .json(response("Success", "Login Success", { token, ...admin }));
    } catch (error) {
      res.status(500).json(response("Fail", error.message));
    }
  }

  static async signup(req, res) {

    const {
      username,
      password
    } = req.body.data

    // Hash password
    const salt = bcrypt.genSaltSync(10);
    const hashedPassword = bcrypt.hashSync(password, salt)

    try {
      // Validasi User
      const {
        error
        } = adminValidation(req.body.data)
        if (error) return res.status(400).json(response("Failed!", error.details[0].message, ""))
        
      const user = await models.Admin.create({
        username,
        password: hashedPassword
      })

      res.status(200).json(response('success', 'success create data', user))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }
}

module.exports = Controller;
