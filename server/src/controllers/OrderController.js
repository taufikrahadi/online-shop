const models = require('../models')
const { Op } = require('sequelize')
const queryOptions = require('../helpers/queryOptions')
const response = require('../helpers/responseTemplate')
const { orderValidation } = require("../helpers/validation");
const { v4: uuidv4 } = require('uuid')
const PaymentController = require('./PaymentController')
const createPdf = require('../helpers/createPdf')
const mustache = require("mustache")
const ejs = require("ejs")
const path = require("path");
const transporter = require("../../config/mailConfig")



module.exports = class OrderStatusController {

  static async fetchAll(req, res) {
    // tangkep query dari client
    const { search, limit, page } = req.query

    // pakai method queryOptions isi argument pertama itu statement searching nya
    // argument ke dua limit
    // argument ke tiga page / offset,
    // argument ke empat include
    /*
    TODO: map include and return object of array
    */
    // const includeProps = include ? include.split(',').map(i => {
    //   const model = models[i]
    //   return { model: model }
    // }) : include
    const options = queryOptions(
      search ? { // ini ternary yg nge cek apakah value search itu != undefined
        [Op.or]: [
          {
            'userId': { [Op.like]: `%${search}%` } // filter search nya dari kolom name
          }, // kalo mau nambah kolom lagi tinggal tambahin selanjutnya
          {
            'total': { [Op.like]: `%${search}%` } 
          },
          {
            'discountId': { [Op.like]: `%${search}%` } 
          },
          {
            'orderShipmentId': { [Op.like]: `%${search}%` } 
          },
          {
            'subTotal': { [Op.like]: `%${search}%` } 
          },
          {
            'postalFee': { [Op.like]: `%${search}%` } 
          },
          {
            'weightTotal': { [Op.like]: `%${search}%` } 
          },
          {
            'orderStatusId': { [Op.like]: `%${search}%` } 
          },
        ]
      } : undefined,
      limit,
      page,
          [
            {
                model: models.User, 
                as: "user",
                attributes: ['firstname', 'lastname', 'username', 'email', 'phonenumber']
            },
            {
                model: models.Discount, 
                as: "discount",
                attributes: ['discountName', 'totalDiscount']
            },
            {
                model: models.OrderStatus, 
                as: "orderStatus",
                attributes: ['name']
            },
            'orderPayment'  
          ]  
    )

    try {
      const orders = await models.Order.findAndCountAll(options) // pake findAndCountAll supaya sekalian nge return total item
      res.status(200)
        .json(
          response(
            'success',
            'data order status',
            {
              data: orders.rows, // data nya
              totalItems: orders.count, // total data nya
              totalPages: page > 0 ? parseInt(orders.count / parseInt(limit)) : 1, // total data dibagi value query limit
              currentPage: page ? parseInt(page) : 1 // current page
            }
          )
        )
    } catch ({ message }) {
      res.json(response(message)).status(500)      
    }
  }

  static async fetchById(req, res) {
    const { id } = req.params
    try {
      const orders = await models.Order.findOne({
        where: {
          id: id
        },
        include: [
          'details',
          'discount',
          'orderShipment',
          'orderPayment',
          'orderStatus',
          'user'
        ]
      })

      if (!orders) return res.status(404).json(response('failed', 'data not found'))
      res.status(200).json(response('success', 'get data by id', orders))
    } catch ({ message }) {
      return res.status(500).json(response('failed', message))
    }
  }

  static async storeData(req, res) {

    try {
      const discount = await models.Discount.findByPk(req.body.data.discountId)
      req.body.data.userId = req.user_id
      let { 
        userId, 
        total, 
        discountId, 
        subTotal, 
        postalFee, 
        weightTotal, 
        provinceId, 
        cityId,
        orderDetails,
        store
      } = req.body.data
      orderDetails = Promise.all(orderDetails.map(async detail => {
        const product = await models.Product.findByPk(detail.productId)
        detail['totalPrice'] = product.price * detail.qty
        detail['weightTotal'] = product.weight * detail.qty
        return detail
      }))
      const details = await orderDetails
      
      subTotal = details.length > 1 ? details.reduce((x,y) => x.totalPrice + y.totalPrice) + postalFee : details[0].totalPrice + postalFee
      weightTotal = details.length > 1 ? details.reduce((x, y) => x.weightTotal + y.weightTotal) : details[0].weightTotal
      total = subTotal - (subTotal * discount.totalDiscount)
      const orders = await models.Order.create({
        userId,
        total,
        discountId,
        subTotal,
        postalFee,
        weightTotal,
        orderShipment: {
          receiptNumber: uuidv4(),
          weightTotal: weightTotal,
          provinceId: provinceId,
          cityId: cityId
        },
        orderPayment: {
          gross_amount: total,
          payment_methods: 'cstore'
        },
        details
      }, {
        include: [
          'orderShipment',
          'orderPayment',
          'details'
        ]
      })

      const paymentTransaction = await PaymentController.storeData(orders, req.body.data)
      console.log(paymentTransaction)
      
      const payment = await models.Payment.findByPk(orders.orderPayment.id)
      console.log(payment.payment_code)
      payment.status = paymentTransaction.transaction_status
      payment.transaction_token = paymentTransaction.transaction_id
      payment.payment_code = paymentTransaction.payment_code
      await payment.save()
    

      const id = `invoice-${new Date().toISOString().split(":").join("-")}`; // nama file berdasar tanggal
      const { count, rows } = await models.OrderDetail.findAndCountAll({
        where: {
          orderId : orders.details[0].orderId
        },
        include:          [
          {
              model: models.Order, as: "order",
              include: [
                {model: models.User, as: "user"}
              ]
          },
          {
              model: models.Product, as: "product"
          },
      ]
      })

      createPdf({count, rows, orders, discount, id: id})
      const template = await ejs.renderFile(path.join(__dirname, "../../views/email-template.ejs"), { payment_code: payment.payment_code, total: orders.total  })
      const mailOptions = {
        from: process.env.EMAIL,
        to: 'lombokvisit98@gmail.com',
        subject: 'INVOICE ORDER',
        template: 'main',
        html: mustache.render(template),
        attachments: [{
          filename: `${id}.pdf`, 
          path: path.join(__dirname, '../../invoice', `${id}.pdf`),
          contentType: 'application/pdf'
        }]

      }
      transporter.sendMail(mailOptions)

      res.status(201).json(response('success', 'success create data', {  orders, payment }))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }



  static async updateData(req, res) {
    const { id } = req.params
    const { 
            total,
            discountId,
            orderShipmentId,
            subTotal,
            postalFee,
            weightTotal,
            orderStatusid 
        } = req.body.data
    

    try {
      const orders = await models.Order.findByPk(id)
      console.log(id)
      if(!orders) return res.json(response('failed', 'data not found')).status(404)
            // Validasi Order
      const {
        error
        } = orderValidation(req.body.data)
        if (error) return res.status(400).json(response("Failed!", error.details[0].message, ""))

        orders.userId = req.user_id
        orders.total = total
        orders.discountId = discountId
        orders.orderShipmentId = orderShipmentId
        orders.subTotal = subTotal
        orders.postalFee = postalFee
        orders.weightTotal = weightTotal
        orders.orderStatusid = orderStatusid
        orders.save()
      res.json(response('success', 'success update data', orders)).status(200)
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }

  static async destroyData(req, res) {
    const { id } = req.params

    try {
      const orders = await models.Order.findByPk(id)
      
      if (!orders) return res.status(404).json(response('failed', 'data not found'))

      await models.Order.destroy({
        where: {
          id: id
        }
      })

      res.status(200).json(response('success', 'data deleted'))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }

  // static async orderAndPayment(req, res) {
  //   const { postalFee, discountId, provinceId, cityId, orderDetails } = req.body.data

  //   const 
  // }
}
