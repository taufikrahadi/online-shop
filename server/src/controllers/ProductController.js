const models = require('../models')
const response = require('../helpers/responseTemplate')
const queryOptions = require('../helpers/queryOptions')
const { Op } = require('sequelize')
const { productValidation } = require('../helpers/validation')
const { v4: uuidv4 } = require('uuid')

class ProductController {
  static async fetchAll(req, res) {
    const { search, limit, page, include } = req.query
    const includeProps = include ? include.split(',') : include
    const options = queryOptions(
      search ? {
        [Op.or]: [
          {
            'name': { [Op.like]: `%${search}%` }
          },
          {
            'description': { [Op.like]: `%${search}%` }
          },
          {
            '$variants.color$': { [Op.like]: `%${search}%` }
          },
          {
            '$tags.tag.name$': { [Op.like]: `%${search}%` }
          },
          {
            '$category.name$': { [Op.like]: `%${search}%` }
          }
        ]
      } : search,
      limit,
      page,
      includeProps
    )

    try {
      const products = await models.Product.findAndCountAll(options)
      const totalPage = page ? products.count / parseInt(limit) : 1
      const total = totalPage % 1 ? parseInt(totalPage) + 1 : totalPage
      res.status(200).json(
        response(
          'success',
          'product data',
          {
            data: products.rows,
            totalItems: products.count,
            totalPages: total,
            currentPage: page ? parseInt(page) : 1
          }
        )
      )
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }

  static async fetchById(req, res) {
    const { id } = req.params
    const { include } = req.query

    const includeProps = include ? include.split(',') : include

    console.log(includeProps)

    const options = queryOptions(
      {
        id: id
      },
      undefined,
      undefined,
      includeProps
    )

    try {
      const product = await models.Product.findOne(options)
      res.status(200).json(response('success', 'get product by id', product))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }

  static async storeData(req, res) {
    const { error } = productValidation(req.body)
    if (error) return res.status(422).json(response('failed', error.details[0].message))
    const file = req.file.path
    req.body.adminId = req.user_id
    req.body.picture = file
    req.body.code = uuidv4()
    try {
      const product = await models.Product.create({ ...req.body })
      res.status(201).json(response('success', 'product created', product))
    } catch ({ message }) {
      res.json(response('failed', message)).status(500)
    }
  }

  static async updateData(req, res) {
    const { id } = req.params
    const { error } = productValidation(req.body)
    if (error) return res.status(422).json(response('failed', error.details[0].message))
    
    const file = req.file.path
    req.body.picture = file
    const { name, price, weight, description, stock, categoryId, picture } = req.body
    try {
      const product = await models.Product.findByPk(id)
      if(!product) return res.status(404).json(response('failed', 'data not found'))

      product.name = name
      product.price = price
      product.weight = weight
      product.description = description
      product.stock = stock
      product.categoryId = categoryId
      product.picture = picture
      await product.save()

      res.status(200).json(response('success', 'product updated', product))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))      
    }
  }

  static async destroyData(req, res) {
    const { id } = req.params
    try {
      const product = await models.Product.findByPk(id)
      if (!product) return res.status(404).json(response('failed', 'data not found'))

      await models.Product.destroy({
        where: {
          id: id
        }
      })
      res.status(200).json(response('success', 'data deleted'))
    } catch ({ message }) {
      res.status(500).json(response('failed', message))
    }
  }
}

module.exports = ProductController
