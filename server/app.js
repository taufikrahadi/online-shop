const express = require('express')
const app = express()
const routerIndex = require('./src/routes/index')
const auth = require('./src/middleware/AuthMiddleware')
const cors = require('cors')
const https = require('https')

app.use('/', routerIndex)
app.use(cors("*"))

const userRoute = require("./src/routes/userRoute")
const adminRoute = require("./src/routes/adminRoute")
const categoryRoute = require("./src/routes/categoryRoute")
const orderRoute = require("./src/routes/orderRoute")
const orderStatusRoute = require('./src/routes/orderStatusRoute')
const tagRoute = require('./src/routes/tagRoute')
const orderDetailRoute = require('./src/routes/orderDetailRoute')
const userAddressRoute = require('./src/routes/userAddressRoute')

const authRoute = require('./src/routes/authRoute')
const discountRoute = require('./src/routes/discounts')
const productRoute = require('./src/routes/productRoute')
const variantRoute = require('./src/routes/variantsRoute')
const productTag = require('./src/routes/productTagRoute')
const paymentRoute = require('./src/routes/paymentRoute')
const rajaOngkirRoute = require('./src/routes/rajaOngkirRoute')
const midtransClient = require('midtrans-client');



app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/api/v1/variant', auth, variantRoute)
app.use('/api/v1/product-tag', auth, productTag)
app.use('/api/v1/address', auth, userAddressRoute)
app.use("/api/v1/user", auth, userRoute)
app.use("/api/v1/category", auth, categoryRoute)
app.use("/api/v1/order", auth, orderRoute)
app.use('/api/v1/order-statuses', auth, orderStatusRoute)
app.use('/api/v1/order-detail', auth, orderDetailRoute)
app.use('/api/v1/tags', auth, tagRoute)
app.use('/api/v1/discount', auth, discountRoute)
app.use('/api/v1/product', productRoute)
app.use('/api/v1/auth', authRoute)
app.use('/api/v1/admin', adminRoute)
app.use('/api/v1/payment', auth, paymentRoute)
app.use('/api/v1/raja-ongkir', rajaOngkirRoute)

  


const host = '0.0.0.0';

const port = process.env.PORT || 3000;

app.listen(port, host, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
