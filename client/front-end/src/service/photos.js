import Api from "./api";

export default {
  get(state) {
    return Api.get("/product", {
      params: { _start: state.start, limit: 15 }
    });
  }
};
