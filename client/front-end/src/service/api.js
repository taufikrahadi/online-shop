import axios from "axios";
import NProgress from "nprogress";
import Vue from "vue";
import store from "@/store/index";

const instance = axios.create({
  baseURL: `http://localhost:3000/api/v1/`,
  headers: {
    "Content-Type": "application/json"
  }
});

instance.interceptors.request.use(
  config => {
    // console.log("storee", store);
    config.headers.Authorization = `bearer ${
      store.state.auth.user.data ? store.state.auth.user.data.token : ""
    }`;
    NProgress.start();
    console.log({ request: config });
    return config;
  },
  error => Promise.reject(error)
);
instance.interceptors.response.use(
  function(response) {
    NProgress.done();
    // console.log(store.state.Settings.toastActive);
    Vue.$toast.success(response.statusText);
    console.log({ response });
    return response;
  },
  function(error) {
    NProgress.done();
    // console.log("xerror",error)
    Vue.$toast.error(error.response.data.message);
    // Vue.$toast.error(error.error);

    return Promise.reject(error.response.data.message);
  }
);

export default instance;
