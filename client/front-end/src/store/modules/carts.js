export default {
  namespaced: true,
  state: () => ({
    carts: []
  }),

  mutations: {
    pushCart(state, payload) {
      state.value = payload;
    }
  },

  actions: {
    addCart({ commit }, item, state) {
      if (!item.selected) {
        item.quantity = 1;
        item.selected = true;
        commit("pushCart", item);
      } else {
        console.log(state.photos);
      }
    }
  }
};
