import Api from "../../service/api";
export default {
  namespaced: true,
  state: () => ({
    user: false
  }),

  mutations: {
    setLogin(state, payload) {
      state.user = payload;
    },
    setLogout(state) {
      state.user = false;
    }
  },

  actions: {
    async reqLogin({ commit }, user) {
      await Api.post("/auth/login", user)
        .then(res => {
          commit("setLogin", res.data);
        })
        .catch(error => console.log({ error }));
    },
    async registerUser({ commit }, user) {
      {
        commit;
      }
      Api.post("/auth/signup", user)
        .then(res => {
          console.log(res);
        })
        .catch(error => console.log({ error }));
    },
    async updateUser({ state, commit }, user) {
      {
        state;
      }
      console.log(user.token);
      user.id = undefined;
      Api.put(`/user`, {
        data: {
          username: user.username,
          firstname: user.firstname,
          lastname: user.lastname,
          email: user.email,
          phonenumber: user.phonenumber,
          password: user.password
        }
      })
        .then(res => {
          console.log(res.data);
          // res.data.token = state.user.data.token

          res.data.data.token = state.user.data.token;
          commit("setLogin", res.data);
          console.log("data", res.data);
        })
        .catch(error => console.log({ error }));
    },

    logout({ commit }) {
      commit("setLogout");
    }
  }
};
