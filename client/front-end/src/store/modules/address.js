import Api from "../../service/api";
import Vue from "vue";
export default {
  namespaced: true,
  state: () => ({
    cities: [],
    citiesName: {},
    provinces: [],
    address: [],
    selectedAddress:{}
  }),

  mutations: {
    setCities(state, payload) {
      state.cities = payload;
    },
    setCitiesAll(state, payload) {
      state.citiesName[payload.id] = payload.data;
    },
    setProvinces(state, payload) {
      state.provinces = payload;
    },
    setAddress(state, payload) {
      state.address = payload;
    },
    setSelected(state,id){
      // state.address = state.address;
      const i = state.address.data.data.findIndex(data => data.id == id)
      state.selectedAddress = state.address.data.data[i]
      console.log(state.selectedAddress)
      // state.address.data.data[i].isSelected = true
    }
  },

  actions: {
    async selectAddress({commit},payload){
      commit("setSelected",payload)
      Vue.$toast.success("Address berhasil diubah");
      console.log("okeeeeeeee",payload)
    },
    async getAddressAll({ commit }) {
      await Api.get("/address/")
        .then(res => {
          console.log(res.data);
          commit("setAddress", res.data);
        })
        .catch(error => console.log({ error }));
    },
    async deleteAddress({ dispatch },id) {
      await Api.delete(`/address/${id}`)
        .then(res => {
          console.log(res.data);
          dispatch("getAddressAll")
        })
        .catch(error => console.log({ error }));
    },
    async getCitiesAll({ commit, state }, id) {
      console.log(state.citiesName[id])
        if (!state.citiesName[id])
        await Api.get(`/raja-ongkir/cities/${id}`)
          .then(res => {
            console.log(res.data);
            commit("setCitiesAll", { id: id, data: res.data.results });
          })
          .catch(error => console.log({ error }));
    },
    async getProvinces({ commit }) {
      await Api.get("/raja-ongkir/provinces/")
        .then(res => {
          console.log(res.data);
          commit("setProvinces", res.data);
        })
        .catch(error => console.log({ error }));
    },
    async getCities({ commit }, id) {
      await Api.get(`/raja-ongkir/cities/${id}`)
        .then(res => {
          console.log(res.data);
          commit("setCities", res.data);
        })
        .catch(error => console.log({ error }));
    },
    async postCities({ commit }, payload) {
      {
        commit;
      }
      await Api.post(`/address`, { data: payload })
        .then(res => {
          console.log(res.data);
        })
        .catch(error => console.log({ error }));
    }
  }
};
