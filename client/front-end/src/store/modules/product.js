import Api from "../../service/api";
import Vue from "vue";
export default {
  namespaced: true,
  state: () => ({
    response: [],
    sresponse: [],
    items: [],
    searchItems: [],
    itemId: [],
    totalItem: 0,
    page: 1,
    spage: 1,
    discount: [],
    ongkir:[]
  }),
  mutations: {
    setProducts(state, payload) {
      state.items = [...state.items, ...payload.data.data.data];
      console.log({ p: state.items, page: state.page });
      state.response = payload.data;
    },
    setItemSearch(state, payload) {
      state.searchItems = payload.data.data.data;
      console.log({ p: state.searchItems, page: state.spage });
      state.sresponse = payload.data;
    },
    setItemId(state, res) {
      state.itemId = res.data.data;
    },
    setDiscount(state, res) {
      state.discount = res.data.data;
    },
    setOngkir(state,res){
      state.ongkir = res
    },

    pushCart(state, item) {
      const i = state.items.findIndex(x => x.id == item);
      if (!state.items[i].selected) {
        Vue.set(state.items[i], "quantity", 1);
        Vue.set(state.items[i], "selected", true);
      } else {
        Vue.set(state.items[i], "quantity", state.items[i].quantity + 1);
      }
    },
    removeCart(state, id) {
      const i = state.items.findIndex(x => x.id == id);
      Vue.set(state.items[i], "quantity", 0);
      Vue.set(state.items[i], "selected", false);
    },
    setCount(state, payload) {
      const i = state.items.findIndex(x => x.id == payload.id);
      if (payload.increase) state.items[i].quantity += 1;
      else {
        state.items[i].quantity -= 1;
        if (state.items[i].quantity < 0) state.items[i].quantity = 0;
      }
      // localStorage.setItem("data", JSON.stringify(state.items));
    }
  },
  actions: {
    increaseCount({ commit }, payload) {
      commit("setCount", payload);
      console.log(payload);
    },
    removeCart({ commit }, id) {
      commit("removeCart", id);
    },
    async getProduct({ commit, state }) {
      await Api.get("/product", {
        params: { limit: 15, page: state.page }
      })
        .then(res => {
          commit("setProducts", { data: res.data });
          if (state.page <= state.response.data.totalPages + 1) {
            state.page++;
          }
        })
        .catch(error => console.log({ error }));
    },
    async getProductId({ commit }, id) {
      await Api.get(`/product/${id}`)
        .then(res => {
          commit("setItemId", { data: res.data });
        })
        .catch(error => console.log({ error }));
    },
    async getDiscount({ commit }) {
      await Api.get(`/discount`)
        .then(res => {
          commit("setDiscount", { data: res.data });
        })
        .catch(error => console.log({ error }));
    },
    async searchProduct({ commit, state }, payload) {
      console.log(payload);
      await Api.get(`/product`, {
        params: {
          search: payload
        }
      })
        .then(res => {
          commit("setItemSearch", { data: res.data });
          if (state.spage <= state.sresponse.data.totalPages + 1) {
            state.spage++;
          }
        })
        .catch(error => console.log({ error }));
    },
    addCart({ commit }, i) {
      commit("pushCart", i);
    },
    async getOngkir({commit},data){
      await Api.post('/raja-ongkir/cost',data)
      .then(res => {
        commit("setOngkir", res.data );
      })
      .catch(error => console.log({ error }));
    }
  }
};
