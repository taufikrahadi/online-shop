import Vue from "vue";
import Vuex from "vuex";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "@/assets/styles/index.css";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import VueToast from "vue-toast-notification";
import VueLazyload from "vue-lazyload";

import VueAwesomeSwiper from "vue-awesome-swiper";
import "swiper/css/swiper.css";
Vue.use(VueAwesomeSwiper);

Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: "/img/logo.png",
  loading: "/img/loading.gif",
  attempt: 1
});
Vue.use(VueLazyload);
Vue.config.productionTip = false;

Vue.use(VueToast, {
  // One of the options
  position: "top-right",
  duration: 5000
});
Vue.$toast.warning("Selamat Datang...");
library.add(fas);
library.add(fab);
Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.use(Vuex);
Vue.filter("capitalize", function(value) {
  return value.replace(/(^\w{1})|(\s{1}\w{1})/g, match => match.toUpperCase());
});
Vue.filter("trimChar", function(words) {
  if (words.length > 50) words = words.slice(0, 50) + "...";

  return words;
});
Vue.filter("formatPrice", function(value) {
  var formatter = new Intl.NumberFormat("ID", {
    style: "currency",
    currency: "IDR",

    // These options are needed to round to whole numbers if that's what you want.
    minimumFractionDigits: 0,
    maximumFractionDigits: 0
  });

  return formatter.format(value); /* $2,500.00 */
  // return "Rp." + (value/1000).toFixed(3);
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
