import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "/login",
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Login.vue"),
    meta: { hideNavigation: true }
  },
  {
    path: "/register",
    name: "Register",
    component: () =>
      import(/* webpackChunkName: "Register" */ "../views/Register.vue"),
    meta: { hideNavigation: true }
  },
  {
    path: "/detail/:id",
    name: "Detail",
    component: () =>
      import(/* webpackChunkName: "Detail" */ "../views/Detail.vue")
  },
  {
    path: "/cart",
    name: "Cart",
    component: () => import(/* webpackChunkName: "Cart" */ "../views/Cart.vue")
  },
  {
    path: "/cart/checkout",
    name: "Checkout",
    component: () =>
      import(/* webpackChunkName: "Checkout" */ "../views/Checkout.vue")
  },
  {
    path: "/search",
    name: "Search",
    component: () =>
      import(/* webpackChunkName: "Search" */ "../views/Search.vue")
  },
  {
    path: "/settings",
    name: "Settings",
    component: () =>
      import(/* webpackChunkName: "Settings" */ "../views/Settings.vue"),
    children: [
      {
        path: "profile",
        component: () =>
          import(
            /* webpackChunkName: "Profile" */ "../views/SettingsProfile.vue"
          )
      },
      {
        path: "profile/edit",
        component: () =>
          import(
            /* webpackChunkName: "Profile" */ "../views/SettingsProfileEdit.vue"
          )
      },
      {
        path: "address",
        component: () =>
          import(
            /* webpackChunkName: "Address" */ "../views/SettingsAddress.vue"
          )
      },
      {
        path: "address/add",
        component: () =>
          import(
            /* webpackChunkName: "addressAdd" */ "../views/SettingsAddressAdd.vue"
          )
      },
      {
        path: "store",
        component: () =>
          import(/* webpackChunkName: "Store" */ "../views/SettingsStore.vue")
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
