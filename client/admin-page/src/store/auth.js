import http from '../plugins/http'
import { toast } from '../plugins/sweetAlert'
import router from '../router/index'
export default {
  namespaced: true,
  state: {
    token: localStorage.getItem('admin_access_token'),
    adminData: {}
  },
  mutations: {
    setToken(state, data) {
      state.token = data
    },
    setAdminData(state, data) {
      state.adminData = data
    }
  },
  actions: {
    async login({ commit }, payload) {
      try {
        const { data } = await http.post('/admin/login', { data: payload })
        commit('setToken', data.data.token)
        localStorage.setItem('admin_access_token', data.data.token)
        toast('Login Success', 'success')
        router.router.push({ name: 'dashboard' })
        return data
      } catch ({ message }) {
        toast('Login Failed', 'error')
        throw new Error(message)
      }
    },
    async logout({ commit }) {
      commit('setToken', null)
      localStorage.removeItem('admin_access_token')
    }
  },
  getters: {
    token: state => state.token,
    adminData: state => state.adminData,
    isLoggedIn: state => {
      return state.token != undefined
    }
  }
}