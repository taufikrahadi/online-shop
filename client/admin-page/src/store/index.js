import Vue from 'vue'
import Vuex from 'vuex'
import productCategory from './productCategory'
import auth from './auth'
import product from './product'
import discount from './discount'
import tag from './tag'
import user from './user'
import order from './order'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    showModal: false,
    isEditing: false,
    isDisable: false
  },
  mutations: {
    setShowModal(state, data) {
      state.showModal = data != undefined ? data : !state.showModal
    },
    setIsEditing(state, data) {
      state.isEditing = data
    },
    setIsDisable(state, data) {
      state.isDisable = data ? data : !state.isDisable
    }
  },
  actions: {
  },
  modules: {
    productCategory,
    auth,
    discount,
    product,
    tag,
    user,
    order
  },
  getters: {
    showModal: state => state.showModal,
    isEditing: state => state.isEditing
  }
})
