import http from '../plugins/http'
import { toast, confirmAlert } from '../plugins/sweetAlert'
http.defaults.headers['Authorization'] = 'Bearer ' + localStorage.getItem('admin_access_token')
export default {
  namespaced: true,
  state: {
    tags: [],
    url: 'tags'
  },
  mutations: {
    setTags(state, data) {
      state.tags = data
    }
  },
  actions: {
    async fetchAll({ commit, state }, query) {
      try {
        const { data } = await http.get(`${state.url}${query ? query : ''}`)
        commit('setTags', data.data)
        return data
      } catch ({ message }) {
        toast('Fetch Data Failed', 'error')
        throw new Error(message)
      }
    },
    async storeData({ dispatch, state, commit }, payload) {
      try {
        const { data } = await http.post(`${state.url}`, { data: payload })
        dispatch('fetchAll', '?limit=10&page=1')
        toast('Data Created', 'success')
        commit('setShowModal', false, { root: true })
        return data
      } catch ({ message }) {
        toast('Failed Created Data', 'error')
        throw new Error(message)
      }
    },
    async updateData({ dispatch, state, commit }, payload) {
      try {
        const url = `${state.url}/${payload.id}`
        payload.id = undefined
        payload.createdAt = undefined
        payload.updatedAt = undefined
        const { data } = await http.put(url, { data: payload })
        dispatch('fetchAll', '?limit=10&page=1')
        toast('Data Updated', 'success')
        commit('setShowModal', false, { root: true })
        return data
      } catch ({ message }) {
        toast('Failed Updating Data', 'error')
        throw new Error(message)
      }
    },
    async destroyData({ dispatch, state }, payload) {
      const conf = await confirmAlert()
      if (conf) {
        try {
          const { data } = await http.delete(`${state.url}/${payload.id}`)
          dispatch('fetchAll', '?limit=10&page=1')
          toast('Data Deleted', 'success')
          return data
        } catch ({ message }) {
          toast('Failed Deleting Data', 'error')
          throw new Error(message)
        }
      }
    }
  },
  getters: {
    tags: state => state.tags
  }
}