import Vue from 'vue'
import DashboardLayout from '@/layout/DashboardLayout'
import AuthLayout from '@/layout/AuthLayout'
import VueRouter from 'vue-router'
import store from '@/store/index'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: 'dashboard',
    component: DashboardLayout,
    children: [
      {
        path: '/dashboard',
        name: 'dashboard',
        component: () => import(/* webpackChunkName: "demo" */ '../views/Dashboard.vue'),
        meta: {
          icons: 'ni ni-tv-2 text-black',
        }
      },
      {
        path: 'products',
        name: 'products',
        component: () => import(/* webpackChunkName: "products" */'../views/Products.vue'),
        meta: {
          icons: 'ni ni-box-2 text-blue'
        },
        children: [
          {
            path: ':id',
            name: 'detail-product',
            component: () => import(/* webpackChunkName: "detail-product" */'../views/Products/Details.vue')
          }
        ]
      },
      {
        path: 'product-categories',
        name: 'product categories',
        component: () => import(/* webpackChunKName: "product-categories" */'../views/ProductCategories.vue'),
        meta: {
          icons: 'ni ni-collection text-orange'
        }
      },
      {
        path: 'tags',
        name: 'tags',
        component: () => import(/* webpackChunkName: "tags" */'../views/Tags.vue'),
        meta: {
          icons: 'ni ni-badge text-pink'
        }
      },
      {
        path: 'users',
        name: 'users',
        component: () => import(/* webpackChunkName: "users" */'../views/Users.vue'),
        meta: {
          icons: 'ni ni-circle-08 text-green'
        }
      },
      {
        path: 'orders',
        name: 'orders',
        component: () => import(/* webpackChunkName: "orders" */'../views/Orders.vue'),
        meta: {
          icons: 'ni ni-shop text-red'
        },
        children: [
          {
            path: ':id',
            name: 'detail-order',
            component: () => import(/* webpackChunkName: "detail-order" */'../views/Orders/Details.vue'),
          }
        ]
      },
      {
        path: 'discounts',
        name: 'discounts',
        component: () => import(/* webpackChunkName: "discounts" */'../views/Discounts.vue'),
        meta: {
          icons: 'ni ni-money-coins text-yellow'
        }
      },
      {
        path: 'admins',
        name: 'admins',
        component: () => import(/* webpackChunkName: "admins" */'../views/Admins.vue'),
        meta: {
          icons: 'ni ni-settings-gear-65 text-teal'
        }
      }
    ],
    beforeEnter: (to, from, next) => {
      if (store.getters['auth/isLoggedIn']) {
        return next()
      } else {
        return next({
          name: 'login'
        })
      }
    }
  },
  {
    path: '/',
    redirect: 'login',
    component: AuthLayout,
    children: [
      {
        path: '/login',
        name: 'login',
        component: () => import(/* webpackChunkName: "demo" */ '../views/Login.vue')
      },
    ],
    beforeEnter: (to, from, next) => {
      if (store.getters['auth/isLoggedIn']) {
        return next({
          name: 'dashboard'
        })
      } else {
        return next()
      }
    }
  },
  {
    path: '*',
    component: () => import('../layout/NotFound.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach

export default { router, routes }
