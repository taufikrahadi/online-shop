import Vue from 'vue'
import VueSweetAlert2 from 'vue-sweetalert2'

Vue.use(VueSweetAlert2)

const toast = (message, icon) => {
  Vue.swal({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    icon: icon,
    title: message,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Vue.swal.stopTimer)
      toast.addEventListener('mouseleave', Vue.swal.resumeTimer)
    }
  })
}

const confirmAlert = async (title = 'Are you sure ?', text = "Delete data cant be retrieved", confirmButtonColor = '#3085d6', cancelButtonColor = '#d33', confirmButtonText = 'Yes, delete it!') => {
  const { value } = await Vue.swal({
    title: title,
    text: text,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: confirmButtonColor,
    cancelButtonColor: cancelButtonColor,
    confirmButtonText: confirmButtonText,
  })
  return value
}

export {
  toast,
  confirmAlert
}
