import axios from 'axios'

const instance = axios.create({
  baseURL: 'http://localhost:3000/api/v1',
  headers: {
    'Content-Type': 'application/json'
  }
})

instance.interceptors.request.use(
  (request) => request,
  (error) => {
      Promise.reject(error)
  }
)

instance.interceptors.response.use(
  (response) => response,
  (error) => Promise.reject(error) 
)

export default instance
