import Vue from 'vue'
import dayjs from 'dayjs'

Vue.filter('capitalize', str => str.split(' ').map(s => s.charAt(0).toUpperCase() + s.slice(1)).join(' '))
Vue.filter('readableDate', val => dayjs(val).format('DD MMMM, YYYY'))
Vue.filter('readableDateTime', val => dayjs(val).format('HH:mm DD MMMM, YYYY'))
Vue.filter('uppercase', val => val.toUpperCase())
Vue.filter('discount', val => (val * 100) + ' %')
Vue.filter('currency', val => new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(val))
